#include "AboutDelaunayEvaluator.h"
#include <QVBoxLayout>
//#include <QStyleOptionTab>
#include "DependencyInfoWidget.h"
#include "mainwindow.h"
AboutDelaunayEvaluator::AboutDelaunayEvaluator(QWidget *parent) :
    QDialog(parent)
{
    this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);
    setWindowTitle(tr("About DelaunayEvaluator"));
    setStyleSheet("background-color:white;");
    setMinimumSize(800,700);
    setMaximumSize(800,700);


    //About DelaunayEvaluator
    QVBoxLayout *mainFrameLayout = new QVBoxLayout();
    QFrame *logoFrame = new QFrame();
    QHBoxLayout *logoFrameLayout = new QHBoxLayout();

    QLabel *lbLogo = new QLabel();
    QPixmap pix(":/img/DelaunayEvaluationImage2.png");
    lbLogo->setPixmap(pix.scaledToWidth(500,Qt::SmoothTransformation));
    lbLogo->setStyleSheet("background:transparent;");
    logoFrameLayout->addStretch();
    logoFrameLayout->addWidget(lbLogo);
    logoFrameLayout->addStretch();

    logoFrame->setLayout(logoFrameLayout);
    mainFrameLayout->addWidget(logoFrame);

    QFrame *infoDelaunayEvaluatorFrame = new QFrame();
    QVBoxLayout *infoDelaunayEvaluatorFrameLayout = new QVBoxLayout();
    QTextEdit *textArea= new QTextEdit();
    textArea->setFrameStyle(QFrame::NoFrame);
    textArea->setReadOnly(true);
    textArea->setText("<h2>DelaunayEvaluation v " + QString( MAINWINDOWDELAUNAYEVALUATOR_VERSION) +" build number "+
                                                                      QString(MAINWINDOWDELAUNAYEVALUATOR_BUILD_NUMBER)  +
                                                                      QString(MAINWINDOWDELAUNAYEVALUATOR_RELEASE_TYPE) + "</h2>"
                          "<font size=\"4\">Tool for the evaluation and comparison of different 2.5D Delaunay triangulation algorithms 3D point clouds.<br><strong>License:</strong> GPL v3</font>"
                          );

    infoDelaunayEvaluatorFrameLayout->addWidget(textArea);

    QFrame *authorsDependenciesFrame = new QFrame();
    QGridLayout *authorsDependenciesFrameLayout = new QGridLayout();
    authorsDependenciesFrameLayout->setContentsMargins(0,0,0,0);

    QLabel *authorsLabel= new QLabel("<h2>Authors</h2>");
    authorsDependenciesFrameLayout->addWidget(authorsLabel,0,0);

    QFrame *authorsFrame = new QFrame();
    QGridLayout *authorsFrameLayout = new QGridLayout();
    authorsFrameLayout->setContentsMargins(0,0,0,0);

    QLabel *lbLogoITOS3D = new QLabel();
    QPixmap pixITOS3D(":/img/ItosLogo.png");
    lbLogoITOS3D->setPixmap(pixITOS3D.scaled(300,300,Qt::KeepAspectRatio,Qt::SmoothTransformation));
    lbLogoITOS3D->setStyleSheet("background:transparent;");


    authorsFrameLayout->addWidget(lbLogoITOS3D,0,1);

    QLabel *usalAuthors = new QLabel("<strong>  Luis L&#0243;pez Fern&#0225;ndez</strong><br/>luisloez89@usal.es");


    authorsFrameLayout->addWidget(usalAuthors,1,1, Qt::AlignTop);

    authorsFrame->setLayout(authorsFrameLayout);
    authorsDependenciesFrameLayout->addWidget(authorsFrame,1,0);

    authorsDependenciesFrame->setLayout(authorsDependenciesFrameLayout);
    infoDelaunayEvaluatorFrameLayout->addWidget(authorsDependenciesFrame);
    infoDelaunayEvaluatorFrame->setLayout(infoDelaunayEvaluatorFrameLayout);

    mainFrameLayout->addWidget(infoDelaunayEvaluatorFrame);

    //Dependencies

    QFrame *infoToolsFrame = new QFrame();
    QVBoxLayout *infoToolsFrameLayout = new QVBoxLayout();

    authorsDependenciesFrameLayout->addWidget(new QLabel("<h2>Dependencies</h2>"),0,1);

    QTabWidget *tabWidgetTools = new QTabWidget;

    tabWidgetTools->addTab(new DependencyInfoWidget("GPL v2","CloudCompare is a 3D point cloud (and triangular mesh) processing software. "
                                                                       "It has been originally designed to perform comparison between two 3D points clouds (such as the ones "
                                                                                            "obtained with a laser scanner) or between a point cloud and a triangular mesh. "
                                                                                            "It relies on a specific octree structure that enables great performances1 in this "
                                                                                            "particular function.","http://www.danielgm.net/cc/"), "CloudCompare");
    tabWidgetTools->addTab(new DependencyInfoWidget("Free of charge for personal non-commercial scientific research","Delaunay Triangulation Library for C++. Fade is among the fastest Delaunay triangulation libraries for C++. Free for scientific research. Commercial licenses and support are available. Easy to use and well documented. Numerically robust. Conforming and Constrained Delaunay triangulation. Delaunay mesh generator. Classic 2D version and extended 2.5D version for height fields","http://www.geom.at/fade2d/html/"), "Fade2D");



    tabWidgetTools->addTab(new DependencyInfoWidget("GPL v3","CGAL is a software project that provides easy access to efficient and reliable geometric algorithms in the form of a C++ library. CGAL is used in various areas needing geometric computation, such as geographic information systems, computer aided design, molecular biology, medical imaging, computer graphics, and robotics.","http://www.cgal.org/index.html"), "CGAL");

    tabWidgetTools->addTab(new DependencyInfoWidget("Commercial licensing","LAStools are the fastest and most memory efficient solution for batch-scripted multi-core LiDAR processing and can turn billions of LiDAR points into useful products at blazing speeds and with low memory requirements.","http://rapidlasso.com/lastools/"), "LasTools");

    tabWidgetTools->addTab(new DependencyInfoWidget("Copyrighted by the author","Triangle generates exact Delaunay triangulations, constrained Delaunay triangulations, conforming Delaunay triangulations, Voronoi diagrams, and high-quality triangular meshes. The latter can be generated with no small or large angles, and are thus suitable for finite element analysis.","https://www.cs.cmu.edu/~quake/triangle.html"), "Triangle");

    tabWidgetTools->addTab(new DependencyInfoWidget("BSD","The Point Cloud Library (PCL) is a standalone, large scale, open project for 2D/3D image and point cloud processing.","http://pointclouds.org/"), "PCL");

    tabWidgetTools->addTab(new DependencyInfoWidget("Copyright (c) 2014, School of Computing, National University of Singapore. All rights reserved.","This program constructs the Delaunay Triangulation of a set of points in 3D using the GPU. The algorithm used is a combination of incremental insertion, flipping and star splaying. The code is written using CUDA programming model of NVIDIA.","https://github.com/ashwin/gDel3D"), "gDel3D");

    infoToolsFrame->setLayout(infoToolsFrameLayout);
    authorsDependenciesFrameLayout->addWidget(tabWidgetTools,1,1);


    setLayout(mainFrameLayout);
}

AboutDelaunayEvaluator::~AboutDelaunayEvaluator()
{
}

