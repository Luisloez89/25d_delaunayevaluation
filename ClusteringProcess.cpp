#include "ClusteringProcess.h"
#include <QFileInfo>
#include <QDir>
#include "liblas/liblas.hpp"
#include <fstream>
#include <iostream>
#include <qmath.h>
// cn_PnPoly(): crossing number test for a point in a polygon

static int cn_PnPoly( point3D P, std::vector<point3D> V, int n )
{
    int    cn = 0;    // the  crossing number counter

    // loop through all edges of the polygon
    for (int i=0; i<n; i++) {    // edge from V[i]  to V[i+1]
       if (((V[i].y <= P.y) && (V[i+1].y > P.y))     // an upward crossing
        || ((V[i].y > P.y) && (V[i+1].y <=  P.y))) { // a downward crossing
            // compute  the actual edge-ray intersect x-coordinate
            float vt = (float)(P.y  - V[i].y) / (V[i+1].y - V[i].y);
            if (P.x <  V[i].x + vt * (V[i+1].x - V[i].x)) // P.x < intersect
                 ++cn;   // a valid crossing of y=P.y right of P.x
        }
    }
    return (cn&1);    // 0 if even (out), and 1 if  odd (in)

}

ClusteringProcess::ClusteringProcess(QString inputFile,int edgeDist):
    mInputFile(inputFile),
    mEdgeDist(edgeDist)
{

}

ClusteringProcess::~ClusteringProcess()
{

}

void ClusteringProcess::run(){

    QFile inputFile(mInputFile);
    //Generate clusters
    mPoints = new std::vector<point3D>();


    // For min/max statistics
    double minx(0.0),miny(0.0),minz(0.0);
    double maxx(0.0),maxy(0.0),maxz(0.0);

    if (QFileInfo(mInputFile).suffix().compare("txt",Qt::CaseInsensitive)==0) {
        if (inputFile.open(QIODevice::ReadOnly))
        {
           bool first = true;
           QTextStream in(&inputFile);
           while (!in.atEnd())
           {
              QString line = in.readLine();
              QStringList splitedLine= line.split(" ");
              double x,y,z;
              x=splitedLine.at(0).toDouble();
              y=splitedLine.at(1).toDouble();
              z=splitedLine.at(2).toDouble();
              point3D p;
              p.x=x;
              p.y=y;
              p.z=z;

              mPoints->push_back(p);
              if (first) {
                  minx=x;
                  miny=y;
                  minz=z;
                  maxx=x;
                  maxy=y;
                  maxz=z;
                  first=false;
              }else {
                  if(x<minx) minx=x;
                  if(y<miny) miny=y;
                  if(z<minz) minz=z;
                  if(x>maxx) maxx=x;
                  if(y>maxy) maxy=y;
                  if(z>maxz) maxz=z;
              }

           }
           inputFile.close();
        }
    }else if (QFileInfo(mInputFile).suffix().compare("las",Qt::CaseInsensitive)==0) {
        std::ifstream ifs;
        ifs.open(mInputFile.toStdString(), std::ios::in | std::ios::binary);
        liblas::ReaderFactory f;
        liblas::Reader reader = f.CreateWithStream(ifs);
        liblas::Header const& header = reader.GetHeader();

        bool first = true;

        while (reader.ReadNextPoint())
        {
            liblas::Point const& pLas = reader.GetPoint();

            point3D p;
            p.x=pLas.GetX();
            p.y=pLas.GetY();
            p.z=pLas.GetZ();
            mPoints->push_back(p);
            if (first) {
                minx=pLas.GetX();
                miny=pLas.GetY();
                minz=pLas.GetZ();
                maxx=pLas.GetX();
                maxy=pLas.GetY();
                maxz=pLas.GetZ();
                first=false;
            }else {
                if(pLas.GetX()<minx) minx=pLas.GetX();
                if(pLas.GetY()<miny) miny=pLas.GetY();
                if(pLas.GetZ()<minz) minz=pLas.GetZ();
                if(pLas.GetX()>maxx) maxx=pLas.GetX();
                if(pLas.GetY()>maxy) maxy=pLas.GetY();
                if(pLas.GetZ()>maxz) maxz=pLas.GetZ();
            }
        }
    }

    //Generate clusters
    mClusterVect = new std::vector<std::vector<point3D>>();
    buildClusterLimits(mClusterVect,minx,maxx,miny,maxy,mEdgeDist);

    //GetGridSize
    int gridX=(int)((maxx-minx)/mEdgeDist);
    int gridY=(int)((maxy-miny)/mEdgeDist);

    mClusters = new std::vector<std::vector<point3D>*>();

    for (int i = 0; i < mClusterVect->size(); ++i) {
        mClusters->push_back(new std::vector<point3D>());
    }


    for (int i = 0; i < mPoints->size(); ++i) {

        int xGridCoord = qFloor((mPoints->at(i).x-minx)/mEdgeDist);
        int yGridCoord = qFloor((mPoints->at(i).y-miny)/mEdgeDist);
        int cellNumber = (gridX*yGridCoord)+xGridCoord;
        mClusters->at(cellNumber)->push_back(mPoints->at(i));
    }

    //Write clustering results .txt.
    QFileInfo inputFileInfo(mInputFile);
    QDir resultsDir(inputFileInfo.absolutePath()+"/"+inputFileInfo.baseName()+"_ClusteringResults");



    for (int i = 0; i < mClusters->size(); ++i) {
        std::vector<point3D> *clusterToSave = mClusters->at(i);
        if (clusterToSave->size()>0) {
            QFileInfo outputFileInfo(resultsDir.absolutePath()+"/"+inputFileInfo.baseName()+"_"+QString::number(i)+".txt");
            std::ofstream outstream;
            outstream.open(outputFileInfo.absoluteFilePath().toStdString(), std::ios::out | std::ios::binary);

            for (int j = 0; j < clusterToSave->size(); ++j) {
                outstream <<  clusterToSave->at(j).x<< " "<<  clusterToSave->at(j).y<< " "<<  clusterToSave->at(j).z<< std::endl;
            }

        }
    }
}

void ClusteringProcess::buildClusterLimits(std::vector<std::vector<point3D>> *clusters,double minX, double maxX, double minY, double maxY, double edgeSize){

    for (double i = minX; i < maxX; i+=edgeSize) {
        for (int j = minY; j < maxY; j+=edgeSize) {
            std::vector<point3D> cluster;
            point3D p;
            p.x=i;
            p.y=j;
            p.z=0;
            cluster.push_back(p);
            p.x=i+edgeSize;
            p.y=j;
            p.z=0;
            cluster.push_back(p);
            p.x=i+edgeSize;
            p.y=j+edgeSize;
            p.z=0;
            cluster.push_back(p);
            p.x=i;
            p.y=j+edgeSize;
            p.z=0;
            cluster.push_back(p);
            p.x=i;
            p.y=j;
            p.z=0;
            cluster.push_back(p);
            clusters->push_back(cluster);
        }
    }
}


