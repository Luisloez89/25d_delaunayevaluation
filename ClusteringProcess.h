#ifndef CLUSTERINGPROCESS_H
#define CLUSTERINGPROCESS_H
#include "ProcessManager/ProcessConcurrent.h"

struct point3D{
    double x;
    double y;
    double z;
};
class ClusteringProcess: public ProcessConcurrent
{
public:
    ClusteringProcess(QString inputFile,int edgeDist);
    ~ClusteringProcess();
    virtual void run();
private:
    QString mInputFile;
    void buildClusterLimits(std::vector<std::vector<point3D>> *clusters,double minX, double maxX, double minY, double maxY, double edgeSize);
    std::vector<std::vector<point3D>> *mClusterVect;
    std::vector<std::vector<point3D>*> *mClusters;
    std::vector<point3D> *mPoints;
    int mEdgeDist;
};

#endif // CLUSTERINGPROCESS_H
