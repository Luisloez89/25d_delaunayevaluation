#ifndef INLIERCHECKER_H
#define INLIERCHECKER_H

#define INF 10000

struct Point
{
    int x;
    int y;
};

class InlierChecker
{
public:
    InlierChecker(Point polygon[], int n);
    ~InlierChecker();

private:
//    Point mPolygon;
//    int nVertices;
//    bool onSegment(Point p, Point q, Point r);
//    int orientation(Point p, Point q, Point r);
//    bool doIntersect(Point p1, Point q1, Point p2, Point q2);
//    bool isInside(Point p);
};

#endif // INLIERCHECKER_H
