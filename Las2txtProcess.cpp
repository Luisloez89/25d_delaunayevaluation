#include "Las2txtProcess.h"
#include <QDir>
#include <QFileInfo>
Las2txtProcess::Las2txtProcess(QString inputFile):
    ExternalProcess(QDir::currentPath()+"/ExternalBinaries/las2txt.exe")
{
    QFileInfo inputFileInfo(inputFile);
    QFileInfo outputFileInfo(inputFileInfo.absolutePath()+"/"+inputFileInfo.baseName()+"_toTxt.txt");

    setWorkingDir(inputFileInfo.absolutePath());

    setStartupMessage("Converting to txt...");



    QStringList inputs;

    inputs << "-i" << inputFileInfo.absoluteFilePath()<<"-parse"<<"xyz" << "-o" << outputFileInfo.absoluteFilePath();

    addIntputs(inputs);
}

Las2txtProcess::~Las2txtProcess()
{

}

