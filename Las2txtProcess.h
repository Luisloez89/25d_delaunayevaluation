#ifndef LAS2TXTPROCESS_H
#define LAS2TXTPROCESS_H
#include "ProcessManager/ExternalProcess.h"


class Las2txtProcess: public ExternalProcess
{
public:
    Las2txtProcess(QString inputFile);
    ~Las2txtProcess();
};

#endif // LAS2TXTPROCESS_H
