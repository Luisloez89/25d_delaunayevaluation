DelaunayEvaluation

* Software for the evaluation and comparison of different triangulation strategies. 

* Dependencies:
- Qt 4.8.6
- Fade2.5D
- PCL
- LibLas
- CGAL
- Relative dependencies to these libreries.

* Compilation tested and working with VS2013 x64

* Release installer and dataset: [Download](https://mega.nz/#!hhhlWaLS!AfxDX5mu_jgfMaTCMGoOKSmg1sAnzqTxqCBAyc9jbZ8)