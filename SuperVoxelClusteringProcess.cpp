#include "SuperVoxelClusteringProcess.h"
#include <pcl/console/parse.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/segmentation/supervoxel_clustering.h>

// Types
typedef pcl::PointXYZ PointT;
typedef pcl::PointCloud<PointT> PointCloudT;

SuperVoxelClusteringProcess::SuperVoxelClusteringProcess(QString inputFile):mInputFile(inputFile)
{

}

SuperVoxelClusteringProcess::~SuperVoxelClusteringProcess()
{

}

void SuperVoxelClusteringProcess::run(){
    PointCloudT::Ptr cloud = boost::shared_ptr <PointCloudT> (new PointCloudT ());
//     if (pcl::io::loadPCDFile<PointT> (mInputFile.toStdString(), *cloud))
//     {
//        return;
//     }
//       float voxel_resolution = 0.008f;
//       float seed_resolution = 0.1f;
//       float color_importance = 0.2f;
//       float spatial_importance = 0.4f;
//       float normal_importance = 1.0f;

//       pcl::SupervoxelClustering<PointT> super (voxel_resolution, seed_resolution);
//       super.setInputCloud (cloud);
//       super.setColorImportance (color_importance);
//       super.setSpatialImportance (spatial_importance);
//       super.setNormalImportance (normal_importance);

//       std::map <uint32_t, pcl::Supervoxel<PointT>::Ptr > supervoxel_clusters;

//       super.extract (supervoxel_clusters);
}
