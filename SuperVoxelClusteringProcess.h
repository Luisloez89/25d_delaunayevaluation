#ifndef SUPERVOXELCLUSTERINGPROCESS_H
#define SUPERVOXELCLUSTERINGPROCESS_H
#include "ProcessManager/ProcessConcurrent.h"


class SuperVoxelClusteringProcess:public ProcessConcurrent
{
public:
    SuperVoxelClusteringProcess(QString inputFile);
    ~SuperVoxelClusteringProcess();
    virtual void run();
private:
    QString mInputFile;
};

#endif // SUPERVOXELCLUSTERINGPROCESS_H
