#-------------------------------------------------
#
# Project created by QtCreator 2016-01-11T10:15:19
#
#-------------------------------------------------

# ensure one "debug_and_release" in CONFIG, for clarity...
debug_and_release {
    CONFIG -= debug_and_release
    CONFIG += debug_and_release
}
# ensure one "debug" or "release" in CONFIG so they can be used as
# conditionals instead of writing "CONFIG(debug, debug|release)"...
CONFIG(debug, debug|release) {
    CONFIG -= debug release
    CONFIG += debug
    }

CONFIG(release, debug|release) {
        CONFIG -= debug release
        CONFIG += release
}

QT       += core gui opengl


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Triang2_5DDelaunayEval
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    ProcessManager/ExternalProcess.cpp \
    ProcessManager/MultiProcess.cpp \
    ProcessManager/Process.cpp \
    ProcessManager/ProcessConcurrent.cpp \
    TriangulationProcess/CGALTriang.cpp \
    ProgressDialog.cpp \
    TriangulationProcess/Las2tinProcess.cpp \
    TriangulationProcess/Fade25DProcess.cpp \
    AboutDelaunayEvaluator.cpp \
    DependencyInfoWidget.cpp \
    TriangleCC/triangle.cpp \
    TriangulationProcess/TriangleCCProcess.cpp \
    ClusteringProcess.cpp \
    TriangulationProcess/GDel3D_GPU_Process.cpp \
    Las2txtProcess.cpp \
    TriangulationProcess/PCLTriangl3DProcess.cpp \
    TriangulationProcess/PCLPoissonProcess.cpp

HEADERS  += mainwindow.h \
    ProcessManager/ExternalProcess.h \
    ProcessManager/MultiProcess.h \
    ProcessManager/Process.h \
    ProcessManager/ProcessConcurrent.h \
    TriangulationProcess/CGALTriang.h \
    ProgressDialog.h \
    TriangulationProcess/Las2tinProcess.h \
    TriangulationProcess/Fade25DProcess.h \
    AboutDelaunayEvaluator.h \
    DependencyInfoWidget.h \
    TriangleProcess/triangle.h \
    ui_mainwindow.h \
    ui_ProgressDialog.h \
    TriangleCC/triangle.h \
    TriangulationProcess/TriangleCCProcess.h \
    ClusteringProcess.h \
    TriangulationProcess/GDel3D_GPU_Process.h \
    Las2txtProcess.h \
    TriangulationProcess/PCLTriangl3DProcess.h \
    TriangulationProcess/PCLPoissonProcess.h

FORMS    += mainwindow.ui \
    ProgressDialog.ui

INCLUDEPATH +=  ../../Libs/CGAL-4.7_vs13_x64/include \
                ../../Libs/gmp_vs13_x64/include \
INCLUDEPATH +=  ../../Libs/boost_1_59_0_vs13_x64/include \
                ../../Libs/fade2.5D1.24_vs13_x64_500000p/include

INCLUDEPATH +=  ../../Libs/libLas_vs13_x64/include
INCLUDEPATH +=  ../../Libs/PCL_1.7.2_vs13_x64/include/pcl-1.7
INCLUDEPATH +=  ../../Libs/Eigen/include
INCLUDEPATH +=  ../../Libs/flann_X64/include


INCLUDEPATH += ../../Libs/libCC_vc13 \
                ../../Libs/libCC_vc13/cc64/qCC/db_tree \
                ../../Libs/libCC_vc13/cc64/CC/include \
                ../../Libs/libCC_vc13/cc64/ccViewer \
                ../../Libs/libCC_vc13/cc64/build/ccViewer \
                ../../Libs/libCC_vc13/cc64/qCC \
                ../../Libs/libCC_vc13/cc64/build/qCC \
                ../../Libs/libCC_vc13/cc64/qCC/ui_templates \
                ../../Libs/libCC_vc13/cc64/qCC/libs/CCFbo \
                ../../Libs/libCC_vc13/cc64/libs/qCC_db \
                ../../Libs/libCC_vc13/cc64/libs/qCC_db/msvc \
                ../../Libs/libCC_vc13/cc64/libs/CCFbo/include

LIBS += -L$$PWD/../../Libs/boost_1_59_0_vs13_x64/lib


debug{
                LIBS += -L$$PWD/../../Libs/libCC_vc13/build64/debug \
                    -L$$PWD/../../Libs/libCC_vc13/cc64/build/qCC/libs/CCFbo/Debug \
                    -L$$PWD/../../Libs/libCC_vc13/cc64/build/CC/Debug \
                    -L$$PWD/../../Libs/libCC_vc13/cc64/build/qCC/db/Debug \
                    -L$$PWD/../../Libs/libCC_vc13/cc64/build/libs/qCC_db/Debug \
                    -L$$PWD/../../Libs/libCC_vc13/cc64/build/CC/triangle/Debug \
                    -L$$PWD/../../Libs/libCC_vc13/cc64/build/libs/Glew/Debug \
                    -L$$PWD/../../Libs/libCC_vc13/cc64/build/libs/CCFbo/Debug \
                    -L$$PWD/../../libs/libLAS/libLAS-1.8.0_x64/bin/Debug \
                    -L$$PWD/../../libs/libLas_vs13_x64/bin/Debug \



LIBS += -L$$PWD/../../Libs/CGAL-4.7_vs13_x64/bin/Debug
LIBS += -L$$PWD/../../Libs/CGAL-4.7_vs13_x64/lib/Debug
LIBS += -L$$PWD/../../Libs/gmp_vs13_x64/lib
LIBS += -L$$PWD/../../Libs/PCL_1.7.2_vs13_x64/lib
LIBS += -L$$PWD/../../Libs/PCL_1.7.2_vs13_x64/bin
LIBS += -L$$PWD/../../Libs/Eigen/bin
LIBS += -L$$PWD/../../flann_X64/bin
LIBS += -L$$PWD/../../flann_X64/lib

LIBS += -lCGAL_Core-vc120-mt-gd-4.7 -lCGAL_ImageIO-vc120-mt-gd-4.7 -lCGAL-vc120-mt-gd-4.7
LIBS += -L$$PWD/../../Libs/fade2.5D1.24_vs13_x64_500000p/bin
LIBS += -lfade25D_x64_v120_Debug
LIBS += -lcc -lQCC_DB_DLLd -lCC_DLLd -lCC_FBOd -lGLEWd -lliblas
LIBS += -llibgmp-10 -llibmpfr-4
LIBS += -lpcl_io_debug -lpcl_common_debug -lpcl_features_debug -lpcl_filters_debug -lpcl_io_ply_debug -lpcl_kdtree_debug -lpcl_keypoints_debug -lpcl_octree_debug -lpcl_recognition_debug -lpcl_registration_debug -lpcl_sample_consensus_debug -lpcl_search_debug -lpcl_segmentation_debug -lpcl_surface_debug#-lmsvcp100 -lmsvcr100

}

release{
            LIBS += -L$$PWD/../../Libs/libCC_vc13/cc64/build/qCC/libs/CCFbo/Release \
                    -L$$PWD/../../Libs/libCC_vc13/cc64/build/CC/Release \
                    -L$$PWD/../../Libs/libCC_vc13/cc64/build/qCC/db/Release \
                    -L$$PWD/../../Libs/libCC_vc13/cc64/build/libs/qCC_db/Release \
                    -L$$PWD/../../Libs/libCC_vc13/cc64/build/CC/triangle/Release \
                    -L$$PWD/../../Libs/libCC_vc13/cc64/build/libs/Glew/Release \
                    -L$$PWD/../../Libs/libCC_vc13/cc64/build/libs/CCFbo/Release \
                    -L$$PWD/../../Libs/libCC_vc13/build64/release  \
                    -L$$PWD/../../libs/libLAS/libLAS-1.8.0_x64/bin/Release \
                    -L$$PWD/../../libs/libLas_vs13_x64/bin/Release \

LIBS += -L$$PWD/../../Libs/CGAL-4.7_vs13_x64/bin/Release
LIBS += -L$$PWD/../../Libs/CGAL-4.7_vs13_x64/lib/Release
LIBS += -L$$PWD/../../Libs/gmp_vs13_x64/lib
LIBS += -L$$PWD/../../Libs/PCL_1.7.2_vs13_x64/lib
LIBS += -L$$PWD/../../Libs/PCL_1.7.2_vs13_x64/bin
LIBS += -L$$PWD/../../Libs/Eigen/bin
LIBS += -L$$PWD/../../flann_X64/bin
LIBS += -L$$PWD/../../flann_X64/lib

LIBS += -lCGAL_Core-vc120-mt-4.7 -lCGAL_ImageIO-vc120-mt-4.7 -lCGAL-vc120-mt-4.7
LIBS += -L$$PWD/../../Libs/fade2.5D1.24_vs13_x64_500000p/bin
LIBS += -lfade25D_x64_v120_Release

LIBS += -lcc -lQCC_DB_DLL -lCC_DLL -lCC_FBO -lGLEW -lliblas
LIBS += -llibgmp-10 -llibmpfr-4
LIBS += -lpcl_io_release -lpcl_common_release -lpcl_features_release -lpcl_filters_release -lpcl_io_ply_release -lpcl_kdtree_release -lpcl_keypoints_release -lpcl_octree_release -lpcl_recognition_release -lpcl_registration_release -lpcl_sample_consensus_release -lpcl_search_release -lpcl_segmentation_release -lpcl_surface_release#-lmsvcp100 -lmsvcr100

}

RESOURCES += ../../Libs/libCC_vc13/cc/qCC/icones.qrc \
    triangulation.qrc

RC_FILE = DelaunayEvaluation.rc
