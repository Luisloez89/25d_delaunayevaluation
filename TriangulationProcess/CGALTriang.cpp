#include "CGALTriang.h"
#include <QFile>
#include <QDir>
#include <QTime>
#include <QMessageBox>
#include <QFutureWatcher>
#include <QtCore>
#include <QThread>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Triangulation_3.h>
#include <CGAL/Delaunay_triangulation_3.h>
#include <CGAL/Constrained_Delaunay_triangulation_2.h>
#include <CGAL/Delaunay_mesh_face_base_2.h>
#include <CGAL/Delaunay_mesher_2.h>
#include <iostream>
#include <fstream>
#include <cassert>
#include <list>
#include <vector>
#include <CGAL/Projection_traits_xy_3.h>
#include <CGAL/Triangulation_conformer_2.h>
#include <CGAL/Delaunay_mesh_size_criteria_2.h>
#include <QMessageBox>
#include <liblas/liblas.hpp>
#include <QTextStream>

// cn_PnPoly(): crossing number test for a point in a polygon

static int cn_PnPoly( CGALPoint P, std::vector<CGALPoint> V, int n )
{
    int    cn = 0;    // the  crossing number counter

    // loop through all edges of the polygon
    for (int i=0; i<n; i++) {    // edge from V[i]  to V[i+1]
       if (((V[i].y() <= P.y()) && (V[i+1].y() > P.y()))     // an upward crossing
        || ((V[i].y() > P.y()) && (V[i+1].y() <=  P.y()))) { // a downward crossing
            // compute  the actual edge-ray intersect x-coordinate
            float vt = (float)(P.y()  - V[i].y()) / (V[i+1].y() - V[i].y());
            if (P.x() <  V[i].x() + vt * (V[i+1].x() - V[i].x())) // P.x < intersect
                 ++cn;   // a valid crossing of y=P.y right of P.x
        }
    }
    return (cn&1);    // 0 if even (out), and 1 if  odd (in)

}

static void readConstraints(std::vector<std::vector<CGALPoint>> *vInConstraints,const char* filename)
{
        QFile inputFile(filename);

        if (inputFile.open(QIODevice::ReadOnly))
        {

           QTextStream in(&inputFile);
           while (!in.atEnd())
           {
              QString line = in.readLine();
              QStringList splitedLine= line.split(",");
              std::vector<CGALPoint> constraintPoints;
              foreach (QString PointCoords, splitedLine) {
                  QStringList coords= PointCoords.split(" ");
                  double x,y,z;
                  x=coords.at(0).toDouble();
                  y=coords.at(1).toDouble();
                  z=coords.at(2).toDouble();
                  constraintPoints.push_back(CGALPoint(x,y,z));
              }
              vInConstraints->push_back(constraintPoints);
           }
           inputFile.close();
        }


}

static void readConstraint(std::vector<CGALPoint>& vInPoints,const char* filename)
{
        QFile inputFile(filename);

        if (inputFile.open(QIODevice::ReadOnly))
        {

           QTextStream in(&inputFile);
           while (!in.atEnd())
           {
              QString line = in.readLine();
              QStringList splitedLine= line.split(" ");
              double x,y,z;
              x=splitedLine.at(0).toDouble();
              y=splitedLine.at(1).toDouble();
              z=splitedLine.at(2).toDouble();
              vInPoints.push_back(CGALPoint(x,y,z));
           }
           inputFile.close();
        }


}

CGALTriang::CGALTriang(QString inputFile,QString constraintFile,QString exclusionFile,bool refineDelaunay,
                       QLabel *lbReadTime,QLabel *lbTrianglTime,QLabel *lbWriteTime,QLabel *lbNumPoints,QLabel *lbNumTriangles,QLabel *lbNumEdges,bool saveResults,bool clusterizationEnabled):
    mInputFile(inputFile),
    mRefineDelaunay(refineDelaunay),
    mConstraintFile(constraintFile),
    mExclusionFile(exclusionFile),
    mLbNumEdges(lbNumEdges),
    mLbNumPoints(lbNumPoints),
    mLbNumTriangles(lbNumTriangles),
    mLbReadTime(lbReadTime),
    mLbTrianglTime(lbTrianglTime),
    mLbWriteTime(lbWriteTime),
    mSaveResults(saveResults),
    mIsClusterizationEnabled(clusterizationEnabled)
{
    QFileInfo resultsFile(QFileInfo(mInputFile).absolutePath()+"/"+"Time_CGAL.txt");
    timeResultsfile = new QFile(resultsFile.absoluteFilePath());
}

CGALTriang::~CGALTriang()
{

}
void CGALTriang::run(){

    //Remove previous results
    QFileInfo inputFileInfo(mInputFile);
    QFileInfo outputFileInfo(inputFileInfo.absolutePath()+"/"+inputFileInfo.baseName()+"_CGALResult.obj");

    CDT cdt;
    QFile inputFile(mInputFile);

    mTime = new QTime();
    mTime->restart();


    //Read exclusions
    mExclusionsVect = new std::vector<std::vector<CGALPoint>>();

    mInPoints = new std::vector<CGALPoint>();

    if (!mExclusionFile.isEmpty()) {


        mExclusionsVect->clear();
        readConstraints(mExclusionsVect,mExclusionFile.toAscii().constData());

        for (int i = 0; i < mExclusionsVect->size(); ++i) {
            cdt.insert_constraint(mExclusionsVect->at(i).begin(),mExclusionsVect->at(i).end());
        }
    }

    if (QFileInfo(mInputFile).suffix().compare("txt",Qt::CaseInsensitive)==0) {
        if (inputFile.open(QIODevice::ReadOnly))
        {
           bool first = true;
           QTextStream in(&inputFile);
           while (!in.atEnd())
           {
              QString line = in.readLine();
              QStringList splitedLine= line.split(" ");
              double x,y,z;
              x=splitedLine.at(0).toDouble();
              y=splitedLine.at(1).toDouble();
              z=splitedLine.at(2).toDouble();
              CGALPoint p(x,y,z);
              bool discardPoint =false;

              if (mExclusionsVect->size()>0) {
                  for (int i = 0; i < mExclusionsVect->size(); ++i) {
                      if (cn_PnPoly(p,mExclusionsVect->at(i),mExclusionsVect->at(i).size()-1)==1) {
                          discardPoint=true;
                      }
                  }
              }
              if (!discardPoint) {
                  mInPoints->push_back(p);
              }
           }
           inputFile.close();
        }
    }else if (QFileInfo(mInputFile).suffix().compare("las",Qt::CaseInsensitive)==0) {
        std::ifstream ifs;
        ifs.open(mInputFile.toStdString(), std::ios::in | std::ios::binary);
        liblas::ReaderFactory f;
        liblas::Reader reader = f.CreateWithStream(ifs);
        liblas::Header const& header = reader.GetHeader();

        while (reader.ReadNextPoint())
        {
            liblas::Point const& pLas = reader.GetPoint();

            CGALPoint p(pLas.GetX(),pLas.GetY(),pLas.GetZ());
            bool discardPoint =false;

            if (mExclusionsVect->size()>0) {
                for (int i = 0; i < mExclusionsVect->size(); ++i) {
                    if (cn_PnPoly(p,mExclusionsVect->at(i),mExclusionsVect->at(i).size()-1)==1) {
                        discardPoint=true;
                    }
                }
            }
            if (!discardPoint) {
                mInPoints->push_back(p);
            }
        }
    }

    int millis, hours, min, seg;
    millis = mTime->elapsed();
    hours = millis/3600000;
    min = (millis-(3600000*hours))/60000;
    seg = (millis-(3600000*hours)-(60000*min))/1000;

    mLbReadTime->setText(QString::number(hours)+" h "+QString::number(min)+" min "+QString::number(seg)+" seg");
    if (!mIsClusterizationEnabled && timeResultsfile->open(QIODevice::ReadWrite |QIODevice::Append)) {
        QTextStream stream(timeResultsfile);
        stream <<QString::number(mInPoints->size()) <<"\t"<<  QString::number(millis) <<"\t";
        timeResultsfile->close();
    }

    mTime->restart();

    //Insert points:
    cdt.insert(mInPoints->begin(),mInPoints->end());

    mInPoints->clear();


    if (!mConstraintFile.isEmpty()) {

        mConstraintsVect =new std::vector<std::vector<CGALPoint>>();
        readConstraints(mConstraintsVect,mConstraintFile.toAscii().constData());

        for (int i = 0; i < mConstraintsVect->size(); ++i) {
            cdt.insert_constraint(mConstraintsVect->at(i).begin(),mConstraintsVect->at(i).end());
        }
    }

    if (mRefineDelaunay) {
        CGAL::refine_Delaunay_mesh_2(cdt,Criteria(0.125, 0.5));
    }

    millis = mTime->elapsed();
    hours = millis/3600000;
    min = (millis-(3600000*hours))/60000;
    seg = (millis-(3600000*hours)-(60000*min))/1000;
    mLbTrianglTime->setText(QString::number(hours)+" h "+QString::number(min)+" min "+QString::number(seg)+" seg");
    if (!mIsClusterizationEnabled && timeResultsfile->open(QIODevice::ReadWrite |QIODevice::Append)) {
        QTextStream stream(timeResultsfile);
        stream <<QString::number(millis) <<"\t";
        timeResultsfile->close();
    }
    mLbNumPoints->setText(QString::number(cdt.number_of_vertices()));
    mLbNumTriangles->setText(QString::number(cdt.number_of_faces()));

    if (mSaveResults) {
        mTime->restart();


        std::ofstream outstream(outputFileInfo.absoluteFilePath().toAscii().constData(),std::ios::out);


        //Write .obj file
        std::map<CDT::Vertex_handle,int> indices;
        int counter = 0;

        outstream << "# " << cdt.number_of_vertices()<< std::endl;
        outstream << "# " << cdt.number_of_faces()<< std::endl;

        for(CDT::Finite_vertices_iterator it = cdt.finite_vertices_begin(); it != cdt.finite_vertices_end(); ++it)
        {
          outstream << "v " << it->point() << std::endl;
          indices.insert(std::pair<CDT::Vertex_handle,int>(it, counter++));
        }

        for(CDT::Finite_faces_iterator it = cdt.finite_faces_begin(); it != cdt.finite_faces_end(); ++it)
        {
          outstream << "f " << indices[it->vertex(0)]+1
                    << " "  << indices[it->vertex(1)]+1
                    << " "  << indices[it->vertex(2)]+1 << std::endl;
        }

        millis = mTime->elapsed();
        hours = millis/3600000;
        min = (millis-(3600000*hours))/60000;
        seg = (millis-(3600000*hours)-(60000*min))/1000;
        mLbWriteTime->setText(QString::number(hours)+" h "+QString::number(min)+" min "+QString::number(seg)+" seg");
        if (!mIsClusterizationEnabled &&timeResultsfile->open(QIODevice::ReadWrite|QIODevice::Append)) {
            QTextStream stream(timeResultsfile);
            stream << QString::number(millis) << endl;
            timeResultsfile->close();
        }
    }else {
        if (!mIsClusterizationEnabled && timeResultsfile->open(QIODevice::ReadWrite|QIODevice::Append)) {
            QTextStream stream(timeResultsfile);
            stream <<  endl;
            timeResultsfile->close();
        }
    }

}
