#ifndef CGALTRIANG_H
#define CGALTRIANG_H


#include <time.h>
#include <iostream>
#include <QDebug>
#include <QTime>
#include <qstringlist.h>
#include "ProcessManager/ProcessConcurrent.h"


#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Triangulation_3.h>
#include <CGAL/Delaunay_triangulation_3.h>
#include <CGAL/Constrained_Delaunay_triangulation_2.h>
#include <CGAL/Delaunay_mesh_face_base_2.h>
#include <CGAL/Delaunay_mesher_2.h>
#include <CGAL/Projection_traits_xy_3.h>
#include <CGAL/Triangulation_conformer_2.h>
#include <CGAL/Delaunay_mesh_size_criteria_2.h>
#include <QLabel>
#include <QFile>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Projection_traits_xy_3<K>  Gt;
typedef CGAL::Triangulation_vertex_base_2<Gt> Vb;
typedef CGAL::Delaunay_mesh_face_base_2<Gt> Fb;
typedef CGAL::Triangulation_data_structure_2<Vb, Fb> Tds;
typedef CGAL::Constrained_Delaunay_triangulation_2<Gt, Tds> CDT;
typedef CGAL::Delaunay_mesh_size_criteria_2<CDT> Criteria;
typedef CDT::Point CGALPoint;



class  CGALTriang : public ProcessConcurrent
{
public:
    CGALTriang(QString inputFile,QString constraintFile,QString exclusionFile,bool refineDelaunay
               ,QLabel *lbReadTime,QLabel *lbTrianglTime,QLabel *lbWriteTime,QLabel *lbNumPoints,QLabel *lbNumTriangles,QLabel *lbNumEdges,bool saveResults,bool clusterizationEnabled);
    ~CGALTriang();

    virtual void run();
private:
    QLabel *mLbReadTime,*mLbTrianglTime,*mLbWriteTime,*mLbNumPoints,*mLbNumTriangles,*mLbNumEdges;
    QTime *mTime;
    QString mInputFile;
    bool mRefineDelaunay,mSaveResults,mIsClusterizationEnabled;
    QString mConstraintFile, mExclusionFile;
    std::vector<std::vector<CGALPoint>> *mExclusionsVect;
    std::vector<std::vector<CGALPoint>> *mConstraintsVect;
    std::vector<std::vector<CGALPoint>> *mClusterVect;
    std::vector<std::vector<CGALPoint>> *mClusterizedData;
    std::vector<CGALPoint> *mInPoints;
    QFile *timeResultsfile;

};
#endif // CGALTRIANG_H
