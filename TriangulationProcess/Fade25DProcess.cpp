#include "Fade25DProcess.h"

#include <QFile>
#include <QDir>
#include <QTime>
#include <QMessageBox>
#include <QFutureWatcher>
#include <QtCore>
#include <QThread>
#include <QImage>
#include <fade25d/Fade_2D.h>
#include <fade25d/Bbox2.h>
#include <liblas/liblas.hpp>
#include <QLabel>
#include <QTextStream>
using namespace std;
using namespace GEOM_FADE25D;

Fade25DProcess::Fade25DProcess(QString inputFile,QString constraintFile,QString exclusionFile,bool keepDelaunay,
                               QLabel *lbReadTime,QLabel *lbTrianglTime,QLabel *lbWriteTime,QLabel *lbNumPoints,QLabel *lbNumTriangles,QLabel *lbNumEdges,bool clusterizationEnabled):
    mConstraintFile(constraintFile),
    mExclusionFile(exclusionFile),
    mKeepDelaunay(keepDelaunay),
      mLbNumEdges(lbNumEdges),
      mLbNumPoints(lbNumPoints),
      mLbNumTriangles(lbNumTriangles),
      mLbReadTime(lbReadTime),
      mLbTrianglTime(lbTrianglTime),
      mLbWriteTime(lbWriteTime),
      mIsClusterizationEnabled(clusterizationEnabled)
{
    mInputFileInfo = new QFileInfo(inputFile);
    mOutputFileInfo = new QFileInfo(mInputFileInfo->absolutePath()+"/"+mInputFileInfo->baseName()+"_Fade25DResult.obj");
    setStartupMessage("Triangulating...");

    QFileInfo resultsFile(mInputFileInfo->absolutePath()+"/"+"Time_Fade25D.txt");
    timeResultsfile = new QFile(resultsFile.absoluteFilePath());
}

Fade25DProcess::~Fade25DProcess()
{

}

void Fade25DProcess::run(){

    mTime = new QTime();
    mTime->restart();

    Fade_2D* pDt=new Fade_2D();
    // 2) Read terrain points from an ASCII file (xyz-coordinates)
    std::vector<Point2> vInputPoints;

    //Read exclusions
    mExclusionsVect = new std::vector<std::vector<Point2>>();

    if (!mExclusionFile.isEmpty()){
        readConstraints(mExclusionsVect,mExclusionFile.toAscii().constData());

        for (int i = 0; i < mExclusionsVect->size(); ++i) {
            if (mExclusionsVect->at(i).size()>2) {
                std::vector<Segment2> vExclusionSegments;
                for (int j = 0; j < mExclusionsVect->at(i).size(); ++j) {

                    if (j!=mExclusionsVect->at(i).size()-1) {
                        Segment2 seg(mExclusionsVect->at(i).at(j),mExclusionsVect->at(i).at(j+1));
                        vExclusionSegments.push_back(seg);
                    }
                }

                ConstraintGraph2* pExclusion;
                if (mKeepDelaunay) {
                    pExclusion=pDt->createConstraint(vExclusionSegments,CIS_KEEP_DELAUNAY);

                }else {
                    pExclusion=pDt->createConstraint(vExclusionSegments,CIS_IGNORE_DELAUNAY);
                }

                pDt->createZone(pExclusion,ZL_OUTSIDE);
            }
        }
    }

    readTerrain(vInputPoints,mInputFileInfo->absoluteFilePath().toAscii().constData(),mExclusionsVect);
    int millis, hours, min, seg;
    millis = mTime->elapsed();
    hours = millis/3600000;
    min = (millis-(3600000*hours))/60000;
    seg = (millis-(3600000*hours)-(60000*min))/1000;
    mLbReadTime->setText(QString::number(hours)+" h "+QString::number(min)+" min "+QString::number(seg)+" seg");

    if (!mIsClusterizationEnabled && timeResultsfile->open(QIODevice::ReadWrite |QIODevice::Append)) {
        QTextStream stream(timeResultsfile);
        stream <<  QString::number(millis) <<"\t";
        timeResultsfile->close();
    }
    mTime->restart();
    // 3) Insert the points
    pDt->insert(vInputPoints);
    //insert constraints
    if(!mConstraintFile.isEmpty()){
       mConstraintsVect =new std::vector<std::vector<Point2>>();
       readConstraints(mConstraintsVect,mConstraintFile.toAscii().constData());

       std::vector<Segment2> vConstraintSegments;

       for (int i = 0; i < mConstraintsVect->size(); ++i) {
           if (mConstraintsVect->at(i).size()>1) {
               for (int j = 1; j < mConstraintsVect->at(i).size(); ++j) {
                   Segment2 seg(mConstraintsVect->at(i).at(j-1),mConstraintsVect->at(i).at(j));
                   vConstraintSegments.push_back(seg);
               }
           }
       }
        ConstraintGraph2* pConstraint;
        if (mKeepDelaunay) {
            pConstraint=pDt->createConstraint(vConstraintSegments,CIS_KEEP_DELAUNAY);
        }else {
            pConstraint=pDt->createConstraint(vConstraintSegments,CIS_IGNORE_DELAUNAY);
        }

        pDt->applyConstraintsAndZones();
    }

    if (mExclusionsVect->size()>0) {
        pDt->applyConstraintsAndZones();
    }

    millis = mTime->elapsed();
    hours = millis/3600000;
    min = (millis-(3600000*hours))/60000;
    seg = (millis-(3600000*hours)-(60000*min))/1000;
    mLbTrianglTime->setText(QString::number(hours)+" h "+QString::number(min)+" min "+QString::number(seg)+" seg");

    if (!mIsClusterizationEnabled && timeResultsfile->open(QIODevice::ReadWrite|QIODevice::Append)) {
        QTextStream stream(timeResultsfile);
        stream << QString::number(millis) <<"\t";
        timeResultsfile->close();
    }

    mTime->restart();

    mLbNumPoints->setText(QString::number(pDt->numberOfPoints()));
    mLbNumTriangles->setText(QString::number(pDt->numberOfTriangles()));

    pDt->writeObj(mOutputFileInfo->absoluteFilePath().toAscii().constData());
    millis = mTime->elapsed();
    hours = millis/3600000;
    min = (millis-(3600000*hours))/60000;
    seg = (millis-(3600000*hours)-(60000*min))/1000;
    mLbWriteTime->setText(QString::number(hours)+" h "+QString::number(min)+" min "+QString::number(seg)+" seg");
    if (!mIsClusterizationEnabled && timeResultsfile->open(QIODevice::ReadWrite|QIODevice::Append)) {
        QTextStream stream(timeResultsfile);
        stream << QString::number(millis) << endl;
        timeResultsfile->close();
    }
}


void Fade25DProcess::readTerrain(std::vector<Point2>& vInPoints,const char* filename, std::vector<std::vector<Point2>>* exclusionsVect)
{
    if (QFileInfo(filename).suffix().compare("txt",Qt::CaseInsensitive)==0) {
        QFile inputFile(filename);
        if (inputFile.open(QIODevice::ReadOnly))
        {
           QTextStream in(&inputFile);
           while (!in.atEnd())
           {
              bool toRemove=false;
              QString line = in.readLine();
              QStringList splitedLine= line.split(" ");
              double x,y,z;
              x=splitedLine.at(0).toDouble();
              y=splitedLine.at(1).toDouble();
              z=splitedLine.at(2).toDouble();

              if (exclusionsVect->size()>0) {
                  for (int i = 0; i < exclusionsVect->size(); ++i) {
                      if (cn_PnPoly(Point2(x,y,0),exclusionsVect->at(i),exclusionsVect->at(i).size()-1)==1) {
                          toRemove=true;
                      }
                  }
              }

              if (!toRemove) {
                  vInPoints.push_back(Point2(x,y,z));
              }
           }
           inputFile.close();

        }
    }else if (QFileInfo(filename).suffix().compare("las",Qt::CaseInsensitive)==0) {
        std::ifstream ifs;
        ifs.open(filename, std::ios::in | std::ios::binary);
        liblas::ReaderFactory f;
        liblas::Reader reader = f.CreateWithStream(ifs);
        liblas::Header const& header = reader.GetHeader();



        while (reader.ReadNextPoint())
        {
            bool toRemove=false;

            liblas::Point const& p = reader.GetPoint();

            if (exclusionsVect->size()>0) {
                for (int i = 0; i < exclusionsVect->size(); ++i) {
                    if (cn_PnPoly(Point2(p.GetX(),p.GetY(),0),exclusionsVect->at(i),exclusionsVect->at(i).size()-1)==1) {
                        toRemove=true;
                    }
                }
            }
            if (!toRemove) {
                vInPoints.push_back(Point2(p.GetX(),p.GetY(),p.GetZ()));
            }
        }
    }

    if (timeResultsfile->open(QIODevice::ReadWrite|QIODevice::Append)) {
        QTextStream stream(timeResultsfile);
        stream << QString::number(vInPoints.size())<<"\t";
        timeResultsfile->close();
    }
}

void Fade25DProcess::readConstraints(std::vector<std::vector<Point2>> *InConstrainst,const char* filename)
{

    QFile inputFile(filename);
    if (inputFile.open(QIODevice::ReadOnly))
    {

       QTextStream in(&inputFile);
       while (!in.atEnd())
       {

           QString line = in.readLine();
           QStringList splitedLine= line.split(",");
           std::vector<Point2> constraintPoints;
           foreach (QString PointCoords, splitedLine) {
               QStringList coords= PointCoords.split(" ");
               double x,y,z;
               x=coords.at(0).toDouble();
               y=coords.at(1).toDouble();
               z=coords.at(2).toDouble();
               constraintPoints.push_back(Point2(x,y,z));
           }
           InConstrainst->push_back(constraintPoints);
       }
       inputFile.close();
    }
}



// cn_PnPoly(): crossing number test for a point in a polygon
int Fade25DProcess::cn_PnPoly( Point2 P, std::vector<Point2> V, int n )
{
    int    cn = 0;    // the  crossing number counter

    // loop through all edges of the polygon
    for (int i=0; i<n; i++) {    // edge from V[i]  to V[i+1]
       if (((V[i].y() <= P.y()) && (V[i+1].y() > P.y()))     // an upward crossing
        || ((V[i].y() > P.y()) && (V[i+1].y() <=  P.y()))) { // a downward crossing
            // compute  the actual edge-ray intersect x-coordinate
            double vt = (double)(P.y()  - V[i].y()) / (V[i+1].y() - V[i].y());
            if (P.x() <  V[i].x() + vt * (V[i+1].x() - V[i].x())) // P.x < intersect
                 ++cn;   // a valid crossing of y=P.y right of P.x
        }
    }
    return (cn&1);    // 0 if even (out), and 1 if  odd (in)

}
