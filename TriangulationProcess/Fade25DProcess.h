#ifndef FADE25DPROCESS_H
#define FADE25DPROCESS_H

#include <time.h>
#include <iostream>
#include <QDebug>
#include <QTime>
#include <qstringlist.h>
#include "ProcessManager/ProcessConcurrent.h"
#include <QFileInfo>
#include <fade25d/Fade_2D.h>
#include <QLabel>
#include <QFile>
#define INF 1000000000

struct Point
{
    double x;
    double y;
};

using namespace std;
using namespace GEOM_FADE25D;

class Fade25DProcess : public ProcessConcurrent
{
public:
    Fade25DProcess(QString inputFile,QString constraintFile,QString exclusionFile,bool ignoreDelaunay,
                   QLabel *lbReadTime,QLabel *lbTrianglTime,QLabel *lbWriteTime,QLabel *lbNumPoints,QLabel *lbNumTriangles,QLabel *lbNumEdges,bool clusterizationEnabled);
    ~Fade25DProcess();
    virtual void run();
    QFileInfo *mInputFileInfo,*mOutputFileInfo;
private:
    QString mConstraintFile, mExclusionFile;
    bool mKeepDelaunay, mIsClusterizationEnabled;
    QLabel *mLbReadTime,*mLbTrianglTime,*mLbWriteTime,*mLbNumPoints,*mLbNumTriangles,*mLbNumEdges;
    QTime *mTime;
    bool isInside(double x,double y);
    void readTerrain(std::vector<Point2>& vInPoints,const char* filename, std::vector<std::vector<Point2>>* exclusionsVect);
    void readConstraints(std::vector<std::vector<Point2>> *InConstrainst,const char* filename);

    std::vector<std::vector<Point2>> *mExclusionsVect;
    std::vector<std::vector<Point2>> *mConstraintsVect;

    int cn_PnPoly( Point2 P, std::vector<Point2> V, int n );
    QFile *timeResultsfile;
};

#endif // FADE25DPROCESS_H
