#include "GDel3D_GPU_Process.h"
#include <QDir>
#include <QFileInfo>
GDel3D_GPU_Process::GDel3D_GPU_Process(QString inputFile):
    ExternalProcess(QDir::currentPath()+"/ExternalBinaries/GDelFlipping.exe")
{
    QFileInfo inputFileInfo(inputFile);
    QFileInfo outputFileInfo(inputFileInfo.absolutePath()+"/Time_gDel3D.txt");

    if (inputFileInfo.suffix().compare("las",Qt::CaseInsensitive)==0) {
        inputFileInfo.setFile(inputFileInfo.absolutePath()+"/"+inputFileInfo.baseName()+"_toTxt.txt");
    }


    setWorkingDir(inputFileInfo.absolutePath());

    setStartupMessage("Triangulating...");


    QStringList inputs;

    inputs <<  inputFileInfo.absoluteFilePath() <<  outputFileInfo.absoluteFilePath();

    addIntputs(inputs);
}

GDel3D_GPU_Process::~GDel3D_GPU_Process()
{

}

