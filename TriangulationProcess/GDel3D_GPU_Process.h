#ifndef GDEL3D_GPU_PROCESS_H
#define GDEL3D_GPU_PROCESS_H
#include "ProcessManager/ExternalProcess.h"


class GDel3D_GPU_Process: public ExternalProcess
{
public:
    GDel3D_GPU_Process(QString inputFile);
    ~GDel3D_GPU_Process();
};

#endif // GDEL3D_GPU_PROCESS_H
