#include "Las2tinProcess.h"
#include <QDir>
#include <QFileInfo>
Las2tinProcess::Las2tinProcess(QString inputFile):
    ExternalProcess(QDir::currentPath()+"/ExternalBinaries/las2tin.exe")
{
    QFileInfo inputFileInfo(inputFile);
    QFileInfo outputFileInfo(inputFileInfo.absolutePath()+"/"+inputFileInfo.baseName()+"_las2tinResult.obj");

    setWorkingDir(inputFileInfo.absolutePath());

    setStartupMessage("Triangulating...");

    QStringList inputs;

    inputs << "-i" << inputFileInfo.absoluteFilePath() << "-o" << outputFileInfo.absoluteFilePath();

    addIntputs(inputs);
}

Las2tinProcess::~Las2tinProcess()
{

}

