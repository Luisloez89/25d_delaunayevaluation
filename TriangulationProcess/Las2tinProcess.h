#ifndef LAS2TINPROCESS_H
#define LAS2TINPROCESS_H
#include "ProcessManager/ExternalProcess.h"


class Las2tinProcess : public ExternalProcess
{
public:
    Las2tinProcess(QString inputFile);
    ~Las2tinProcess();
};

#endif // LAS2TINPROCESS_H
