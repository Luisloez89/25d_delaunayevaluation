#include "PCLPoissonProcess.h"
#include <QFileInfo>
#include <QTime>

#include <pcl/common/common.h>
#include <pcl/search/kdtree.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/point_types.h>
#include <pcl/surface/mls.h>
#include <pcl/surface/poisson.h>
#include <pcl/io/obj_io.h>
#include "liblas/liblas.hpp"

using namespace pcl;

PCLPoissonProcess::PCLPoissonProcess(QString inputFile,QLabel *lbReadTime,QLabel *lbTrianglTime,QLabel *lbWriteTime,QLabel *lbNumPoints,QLabel *lbNumTriangles,QLabel *lbNumEdges,bool saveResults):
    mInputFile(inputFile),
    mLbNumEdges(lbNumEdges),
    mLbNumPoints(lbNumPoints),
    mLbNumTriangles(lbNumTriangles),
    mLbReadTime(lbReadTime),
    mLbTrianglTime(lbTrianglTime),
    mLbWriteTime(lbWriteTime),
    mSaveResults(saveResults)
{

}

PCLPoissonProcess::~PCLPoissonProcess()
{

}

void PCLPoissonProcess::run(){
    QFileInfo inputFileInfo(mInputFile);
    QFileInfo outputFileInfo(inputFileInfo.absolutePath()+"/"+inputFileInfo.baseName()+"_PCLPoissonResult.obj");


    QFile inputFile(mInputFile);

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);

    mTime = new QTime();
    mTime->restart();

    if (QFileInfo(mInputFile).suffix().compare("txt",Qt::CaseInsensitive)==0) {
        if (inputFile.open(QIODevice::ReadOnly))
        {
           QTextStream in(&inputFile);
           while (!in.atEnd())
           {
              QString line = in.readLine();
              QStringList splitedLine= line.split(" ");
              double x,y,z;
              x=splitedLine.at(0).toDouble();
              y=splitedLine.at(1).toDouble();
              z=splitedLine.at(2).toDouble();
              pcl::PointXYZ p(x,y,z);
              cloud->push_back(p);
           }
           inputFile.close();
        }
    }else if (QFileInfo(mInputFile).suffix().compare("las",Qt::CaseInsensitive)==0) {
        std::ifstream ifs;
        ifs.open(mInputFile.toStdString(), std::ios::in | std::ios::binary);
        liblas::ReaderFactory f;
        liblas::Reader reader = f.CreateWithStream(ifs);
        liblas::Header const& header = reader.GetHeader();

        while (reader.ReadNextPoint())
        {
            liblas::Point const& pLas = reader.GetPoint();
            pcl::PointXYZ p(pLas.GetX(),pLas.GetY(),pLas.GetZ());
            cloud->push_back(p);
        }
    }
    int millis, hours, min, seg;
    millis = mTime->elapsed();
    hours = millis/3600000;
    min = (millis-(3600000*hours))/60000;
    seg = (millis-(3600000*hours)-(60000*min))/1000;

    mLbReadTime->setText(QString::number(hours)+" h "+QString::number(min)+" min "+QString::number(seg)+" seg");

    mTime->restart();


    // Normal estimation*
         NormalEstimationOMP<PointXYZ, Normal> ne;
         ne.setNumberOfThreads(8);
         ne.setInputCloud(cloud);
         ne.setRadiusSearch(0.01);
         Eigen::Vector4f centroid;
         compute3DCentroid(*cloud, centroid);
         ne.setViewPoint(centroid[0], centroid[1], centroid[2]);

         PointCloud<Normal>::Ptr cloud_normals (new PointCloud<Normal>());
         ne.compute(*cloud_normals);

         for(size_t i = 0; i < cloud_normals->size(); ++i){
           cloud_normals->points[i].normal_x *= -1;
           cloud_normals->points[i].normal_y *= -1;
           cloud_normals->points[i].normal_z *= -1;
         }

         PointCloud<PointNormal>::Ptr cloud_smoothed_normals(new PointCloud<PointNormal>());
         concatenateFields(*cloud, *cloud_normals, *cloud_smoothed_normals);

         Poisson<PointNormal> poisson;
         poisson.setDepth(9);
         poisson.setInputCloud(cloud_smoothed_normals);
         PolygonMesh mesh;
         poisson.reconstruct(mesh);


    millis = mTime->elapsed();
    hours = millis/3600000;
    min = (millis-(3600000*hours))/60000;
    seg = (millis-(3600000*hours)-(60000*min))/1000;
    mLbTrianglTime->setText(QString::number(hours)+" h "+QString::number(min)+" min "+QString::number(seg)+" seg");

    if (mSaveResults) {
        mTime->restart();
        pcl::io::saveOBJFile(outputFileInfo.absoluteFilePath().toAscii().constData(),mesh);


        millis = mTime->elapsed();
        hours = millis/3600000;
        min = (millis-(3600000*hours))/60000;
        seg = (millis-(3600000*hours)-(60000*min))/1000;
        mLbWriteTime->setText(QString::number(hours)+" h "+QString::number(min)+" min "+QString::number(seg)+" seg");

    }else {

    }
}
