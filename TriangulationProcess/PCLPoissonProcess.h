#ifndef PCLPOISSONPROCESS_H
#define PCLPOISSONPROCESS_H
#include "ProcessManager/ProcessConcurrent.h"
#include <QLabel>
#include <QFile>

class PCLPoissonProcess: public ProcessConcurrent
{
public:
    PCLPoissonProcess(QString inputFile,QLabel *lbReadTime,QLabel *lbTrianglTime,QLabel *lbWriteTime,QLabel *lbNumPoints,QLabel *lbNumTriangles,QLabel *lbNumEdges,bool saveResults);
    ~PCLPoissonProcess();
    virtual void run();
private:
    QLabel *mLbReadTime,*mLbTrianglTime,*mLbWriteTime,*mLbNumPoints,*mLbNumTriangles,*mLbNumEdges;
    QTime *mTime;
    QString mInputFile;
    QFile *timeResultsfile;
    bool mSaveResults;
};

#endif // PCLPOISSONPROCESS_H
