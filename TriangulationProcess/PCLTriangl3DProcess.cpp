#include "PCLTriangl3DProcess.h"
#include <QFileInfo>
#include <QTime>

#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/features/normal_3d.h>
#include <pcl/surface/gp3.h>
#include <pcl/io/obj_io.h>
#include "liblas/liblas.hpp"
PCLTriangl3DProcess::PCLTriangl3DProcess(QString inputFile,QLabel *lbReadTime,QLabel *lbTrianglTime,QLabel *lbWriteTime,QLabel *lbNumPoints,QLabel *lbNumTriangles,QLabel *lbNumEdges,bool saveResults):
    mInputFile(inputFile),
    mLbNumEdges(lbNumEdges),
    mLbNumPoints(lbNumPoints),
    mLbNumTriangles(lbNumTriangles),
    mLbReadTime(lbReadTime),
    mLbTrianglTime(lbTrianglTime),
    mLbWriteTime(lbWriteTime),
    mSaveResults(saveResults)
{

}

PCLTriangl3DProcess::~PCLTriangl3DProcess()
{

}

void PCLTriangl3DProcess::run(){
    QFileInfo inputFileInfo(mInputFile);
    QFileInfo outputFileInfo(inputFileInfo.absolutePath()+"/"+inputFileInfo.baseName()+"_PCL3DResult.obj");


    QFile inputFile(mInputFile);

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);

    mTime = new QTime();
    mTime->restart();

    if (QFileInfo(mInputFile).suffix().compare("txt",Qt::CaseInsensitive)==0) {
        if (inputFile.open(QIODevice::ReadOnly))
        {
           QTextStream in(&inputFile);
           while (!in.atEnd())
           {
              QString line = in.readLine();
              QStringList splitedLine= line.split(" ");
              double x,y,z;
              x=splitedLine.at(0).toDouble();
              y=splitedLine.at(1).toDouble();
              z=splitedLine.at(2).toDouble();
              pcl::PointXYZ p(x,y,z);
              cloud->push_back(p);
           }
           inputFile.close();
        }
    }else if (QFileInfo(mInputFile).suffix().compare("las",Qt::CaseInsensitive)==0) {
        std::ifstream ifs;
        ifs.open(mInputFile.toStdString(), std::ios::in | std::ios::binary);
        liblas::ReaderFactory f;
        liblas::Reader reader = f.CreateWithStream(ifs);
        liblas::Header const& header = reader.GetHeader();

        while (reader.ReadNextPoint())
        {
            liblas::Point const& pLas = reader.GetPoint();
            pcl::PointXYZ p(pLas.GetX(),pLas.GetY(),pLas.GetZ());
            cloud->push_back(p);
        }
    }
    int millis, hours, min, seg;
    millis = mTime->elapsed();
    hours = millis/3600000;
    min = (millis-(3600000*hours))/60000;
    seg = (millis-(3600000*hours)-(60000*min))/1000;

    mLbReadTime->setText(QString::number(hours)+" h "+QString::number(min)+" min "+QString::number(seg)+" seg");


    mTime->restart();

    // Normal estimation*
     pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> n;
     pcl::PointCloud<pcl::Normal>::Ptr normals (new pcl::PointCloud<pcl::Normal>);
     pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
     tree->setInputCloud (cloud);
     n.setInputCloud (cloud);
     n.setSearchMethod (tree);
     n.setKSearch (20);
     n.compute (*normals);

     // Concatenate the XYZ and normal fields*
     pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals (new pcl::PointCloud<pcl::PointNormal>);
     pcl::concatenateFields (*cloud, *normals, *cloud_with_normals);

     // Create search tree*
     pcl::search::KdTree<pcl::PointNormal>::Ptr tree2 (new pcl::search::KdTree<pcl::PointNormal>);
     tree2->setInputCloud (cloud_with_normals);

     // Initialize objects
     pcl::GreedyProjectionTriangulation<pcl::PointNormal> gp3;
     pcl::PolygonMesh triangles;

     // Set the maximum distance between connected points (maximum edge length)
     gp3.setSearchRadius (1);

     // Set typical values for the parameters
     gp3.setMu (2.5);
     gp3.setMaximumNearestNeighbors (100);
     gp3.setMaximumSurfaceAngle(M_PI/4); // 45 degrees
     gp3.setMinimumAngle(M_PI/18); // 10 degrees
     gp3.setMaximumAngle(2*M_PI/3); // 120 degrees
     gp3.setNormalConsistency(false);

     // Get result
     gp3.setInputCloud (cloud_with_normals);
     gp3.setSearchMethod (tree2);
     gp3.reconstruct (triangles);



    millis = mTime->elapsed();
    hours = millis/3600000;
    min = (millis-(3600000*hours))/60000;
    seg = (millis-(3600000*hours)-(60000*min))/1000;
    mLbTrianglTime->setText(QString::number(hours)+" h "+QString::number(min)+" min "+QString::number(seg)+" seg");

    if (mSaveResults) {
        mTime->restart();
        pcl::io::saveOBJFile(outputFileInfo.absoluteFilePath().toAscii().constData(),triangles);


        millis = mTime->elapsed();
        hours = millis/3600000;
        min = (millis-(3600000*hours))/60000;
        seg = (millis-(3600000*hours)-(60000*min))/1000;
        mLbWriteTime->setText(QString::number(hours)+" h "+QString::number(min)+" min "+QString::number(seg)+" seg");

    }else {

    }
}
