#ifndef PCLTRIANGL3DPROCESS_H
#define PCLTRIANGL3DPROCESS_H
#include "ProcessManager/ProcessConcurrent.h"
#include <QLabel>
#include <QFile>
class PCLTriangl3DProcess: public ProcessConcurrent
{
public:
    PCLTriangl3DProcess(QString inputFile,QLabel *lbReadTime,QLabel *lbTrianglTime,QLabel *lbWriteTime,QLabel *lbNumPoints,QLabel *lbNumTriangles,QLabel *lbNumEdges,bool saveResults);
    ~PCLTriangl3DProcess();
    virtual void run();
private:
    QLabel *mLbReadTime,*mLbTrianglTime,*mLbWriteTime,*mLbNumPoints,*mLbNumTriangles,*mLbNumEdges;
    QTime *mTime;
    QString mInputFile;
    QFile *timeResultsfile;
    bool mSaveResults;
};

#endif // PCLTRIANGL3DPROCESS_H
