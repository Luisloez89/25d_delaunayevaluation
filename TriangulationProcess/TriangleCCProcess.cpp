#include "TriangleCCProcess.h"
#include "TriangleCC/triangle.h"
#include <iostream>
#include <fstream>
#include <liblas/liblas.hpp>
#include <QTextStream>

TriangleCCProcess::TriangleCCProcess(QString inputFile,
                                     QLabel *lbReadTime,QLabel *lbTrianglTime,QLabel *lbWriteTime,QLabel *lbNumPoints,QLabel *lbNumTriangles,QLabel *lbNumEdges,bool clusterizationEnabled):
    mLbNumEdges(lbNumEdges),
    mLbNumPoints(lbNumPoints),
    mLbNumTriangles(lbNumTriangles),
    mLbReadTime(lbReadTime),
    mLbTrianglTime(lbTrianglTime),
    mLbWriteTime(lbWriteTime),
    mIsClusterizationEnabled(clusterizationEnabled)
{
    mInputFileInfo = new QFileInfo(inputFile);
    QFileInfo resultsFile(mInputFileInfo->absolutePath()+"/"+"Time_Triangle.txt");
    timeResultsfile = new QFile(resultsFile.absoluteFilePath());
}

TriangleCCProcess::~TriangleCCProcess()
{

}

void TriangleCCProcess::run(){

    mTime = new QTime();
    mTime->restart();

    std::vector<TrianglePoint> vInPoints;
    std::vector<Point3D> v3DInPoints;
    readTerrain(vInPoints,v3DInPoints,mInputFileInfo->absoluteFilePath().toAscii());

    int millis, hours, min, seg;
    millis = mTime->elapsed();
    hours = millis/3600000;
    min = (millis-(3600000*hours))/60000;
    seg = (millis-(3600000*hours)-(60000*min))/1000;
    mLbReadTime->setText(QString::number(hours)+" h "+QString::number(min)+" min "+QString::number(seg)+" seg");

    if (!mIsClusterizationEnabled && timeResultsfile->open(QIODevice::ReadWrite |QIODevice::Append)) {
        QTextStream stream(timeResultsfile);
        stream <<QString::number(vInPoints.size()) <<"\t"<<  QString::number(millis) <<"\t";
        timeResultsfile->close();
    }
    mTime->restart();

    mLbNumPoints->setText(QString::number(vInPoints.size()));


    triangulateio in;
    memset(&in,0,sizeof(triangulateio));
    in.numberofpoints = static_cast<int>(vInPoints.size());

    in.numberofpointattributes = 0;
    in.pointlist = (float *) malloc(in.numberofpoints * 2 * sizeof(float));
    in.pointmarkerlist = (int *) malloc(in.numberofpoints * sizeof(int));
    for (int i = 0; i < in.numberofpoints; ++i) {
        in.pointlist[i*2]=vInPoints.at(i).x;
        in.pointlist[(i*2)+1]=vInPoints.at(i).y;
        in.pointmarkerlist[i]=0;
    }
    in.numberofsegments = 0;
    in.numberofholes = 0;
    in.numberofregions = 0;

    triangulateio out;
    memset(&out,0,sizeof(triangulateio));

    triangulate ( "pczBPNIOQY", &in, &out, 0 );


    int m_numberOfTriangles = out.numberoftriangles;
    if (m_numberOfTriangles > 0)
    {
        int* m_triIndexes = out.trianglelist;

        //remove non existing points
        int* _tri = out.trianglelist;
        for (int i=0; i<out.numberoftriangles; )
        {
            if (	_tri[0] >= in.numberofpoints
                ||	_tri[1] >= in.numberofpoints
                ||	_tri[2] >= in.numberofpoints)
            {
                int lasTriIndex = (out.numberoftriangles-1) * 3;
                _tri[0] = out.trianglelist[lasTriIndex + 0];
                _tri[1] = out.trianglelist[lasTriIndex + 1];
                _tri[2] = out.trianglelist[lasTriIndex + 2];
                --out.numberoftriangles;
            }
            else
            {
                _tri += 3;
                ++i;
            }
        }

        //Reduce memory size
        if (out.numberoftriangles < static_cast<int>(m_numberOfTriangles))
        {
            realloc(m_triIndexes, sizeof(int)*out.numberoftriangles*3);
            m_numberOfTriangles = out.numberoftriangles;
        }
    }

    millis = mTime->elapsed();
    hours = millis/3600000;
    min = (millis-(3600000*hours))/60000;
    seg = (millis-(3600000*hours)-(60000*min))/1000;
    mLbTrianglTime->setText(QString::number(hours)+" h "+QString::number(min)+" min "+QString::number(seg)+" seg");
    if (!mIsClusterizationEnabled && timeResultsfile->open(QIODevice::ReadWrite |QIODevice::Append)) {
        QTextStream stream(timeResultsfile);
        stream <<  QString::number(millis) <<"\t";
        timeResultsfile->close();
    }
    mTime->restart();

    mLbNumTriangles->setText(QString::number(out.numberoftriangles));

    //Writeresults
    QFileInfo outputFileInfo(mInputFileInfo->absolutePath()+"/"+mInputFileInfo->baseName()+"_TriangleCCResult.obj");

    std::ofstream outstream(outputFileInfo.absoluteFilePath().toUtf8().constData(),std::ios::out);


    //Write .obj file
    outstream << "# " << out.numberofpoints<< std::endl;
    outstream << "# " << out.numberoftriangles<< std::endl;



    for (int i = 0; i < v3DInPoints.size(); i++)
    {
        outstream << "v " << v3DInPoints.at(i).x << " " << v3DInPoints.at(i).y << " " << v3DInPoints.at(i).z << std::endl;
    }

    for (int i = 0; i < out.numberoftriangles; i++) {
        outstream << "f " << out.trianglelist[i * out.numberofcorners + 0]+1 << " " << out.trianglelist[i * out.numberofcorners + 1]+1 << " " << out.trianglelist[i * out.numberofcorners + 2]+1 << std::endl;

    }
    trifree(out.segmentmarkerlist);
    trifree(out.segmentlist);
    millis = mTime->elapsed();
    hours = millis/3600000;
    min = (millis-(3600000*hours))/60000;
    seg = (millis-(3600000*hours)-(60000*min))/1000;
    mLbWriteTime->setText(QString::number(hours)+" h "+QString::number(min)+" min "+QString::number(seg)+" seg");
    if (!mIsClusterizationEnabled && timeResultsfile->open(QIODevice::ReadWrite |QIODevice::Append)) {
        QTextStream stream(timeResultsfile);
        stream <<  QString::number(millis) << endl;
        timeResultsfile->close();
    }

}


void TriangleCCProcess::readTerrain(std::vector<TrianglePoint> &vInPoints,std::vector<Point3D> &v3DInPoints, const char *filename){

    // For min/max statistics
    double minx(DBL_MAX),miny(DBL_MAX),minz(DBL_MAX);
    double maxx(DBL_MIN),maxy(DBL_MIN),maxz(DBL_MIN);

    if (QFileInfo(filename).suffix().compare("txt",Qt::CaseInsensitive)==0) {
        QFile inputFile(filename);
        if (inputFile.open(QIODevice::ReadOnly))
        {

           QTextStream in(&inputFile);
           while (!in.atEnd())
           {
              QString line = in.readLine();
              QStringList splitedLine= line.split(" ");
              double x,y,z;
              x=splitedLine.at(0).toDouble();
              y=splitedLine.at(1).toDouble();
              z=splitedLine.at(2).toDouble();

                TrianglePoint p;
                p.x=x;
                p.y=y;
                Point3D p3d;
                p3d.x=x;
                p3d.y=y;
                p3d.z=z;
                v3DInPoints.push_back(p3d);
                vInPoints.push_back(p);
                if(x<minx) minx=x;
                if(y<miny) miny=y;
                if(z<minz) minz=z;
                if(x>maxx) maxx=x;
                if(y>maxy) maxy=y;
                if(z>maxz) maxz=z;
           }
           inputFile.close();

        }
    }else if (QFileInfo(filename).suffix().compare("las",Qt::CaseInsensitive)==0) {
        std::ifstream ifs;
        ifs.open(filename, std::ios::in | std::ios::binary);
        liblas::ReaderFactory f;
        liblas::Reader reader = f.CreateWithStream(ifs);


        while (reader.ReadNextPoint())
        {

            liblas::Point const& pLas = reader.GetPoint();


            TrianglePoint p;
            p.x=pLas.GetX();
            p.y=pLas.GetY();
            Point3D p3d;
            p3d.x=pLas.GetX();
            p3d.y=pLas.GetY();
            p3d.z=pLas.GetZ();
            v3DInPoints.push_back(p3d);
            vInPoints.push_back(p);
            if(pLas.GetX()<minx) minx=pLas.GetX();
            if(pLas.GetY()<miny) miny=pLas.GetY();
            if(pLas.GetZ()<minz) minz=pLas.GetZ();
            if(pLas.GetX()>maxx) maxx=pLas.GetX();
            if(pLas.GetY()>maxy) maxy=pLas.GetY();
            if(pLas.GetZ()>maxz) maxz=pLas.GetZ();
        }
    }

}
