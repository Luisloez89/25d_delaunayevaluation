#ifndef TRIANGLECCPROCESS_H
#define TRIANGLECCPROCESS_H
#include "ProcessManager/ProcessConcurrent.h"
#include <QFileInfo>
#include <QLabel>
#include <QTime>
#include <QFile>
struct TrianglePoint{
    double x;
    double y;
};
struct Point3D{
    double x;
    double y;
    double z;
};
class TriangleCCProcess: public ProcessConcurrent
{
public:
    TriangleCCProcess(QString inputFile,
                      QLabel *lbReadTime,QLabel *lbTrianglTime,QLabel *lbWriteTime,QLabel *lbNumPoints,QLabel *lbNumTriangles,QLabel *lbNumEdges,bool clusterizationEnabled);
    ~TriangleCCProcess();
    virtual void run();
private:
    bool mIsClusterizationEnabled;
    QLabel *mLbReadTime,*mLbTrianglTime,*mLbWriteTime,*mLbNumPoints,*mLbNumTriangles,*mLbNumEdges;
    QTime *mTime;
    QFileInfo *mInputFileInfo,*mOutputFileInfo;
    void readTerrain(std::vector<TrianglePoint>& vInPoints,std::vector<Point3D> &v3DInPoints,const char* filename);
    QFile *timeResultsfile;
};

#endif // TRIANGLECCPROCESS_H
