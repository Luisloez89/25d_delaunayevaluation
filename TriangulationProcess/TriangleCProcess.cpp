#include "TriangleCProcess.h"
#include <QMessageBox>
//#include "triangle.h"
TriangleCProcess::TriangleCProcess(QString inputFile,QStringList constraintFileList,QStringList exclusionFileList):
    mConstraintFileList(constraintFileList),
    mExclusionFileList(exclusionFileList)
{
    mInputFileInfo = new QFileInfo(inputFile);
    mOutputFileInfo = new QFileInfo(mInputFileInfo->absolutePath()+"/"+mInputFileInfo->baseName()+"_TriangleppResult.obj");
    setStartupMessage("Triangulating...");
}

TriangleCProcess::~TriangleCProcess()
{

}

void TriangleCProcess::run(){

    //Remove previous results
    if (mOutputFileInfo->exists()) {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Remove previous results");
        msgBox.setIconPixmap(QPixmap(":/img/trianglIcon.png"));
        msgBox.setText("Current results will be removed.");
        msgBox.setInformativeText("Do you want to continue?");
        msgBox.setStandardButtons(QMessageBox::Cancel | QMessageBox::Ok);
        msgBox.setDefaultButton(QMessageBox::Ok);
        int ret = msgBox.exec();
        if (ret!= QMessageBox::Ok) {
            return;
        }
        QFile(mOutputFileInfo->absoluteFilePath()).remove();
    }
    std::vector<Point> vInputPoints;
    readTerrain(vInputPoints,mInputFileInfo->absoluteFilePath().toAscii().constData());

    struct triangulateio in, mid, out, vorout;

      /* Define input points. */

      in.numberofpoints = vInputPoints.size();
      in.numberofpointattributes = 0;
      in.pointlist = (float *) malloc(in.numberofpoints * 2 * sizeof(float));
      in.pointmarkerlist = (int *) malloc(in.numberofpoints * sizeof(int));
      for (int i = 0; i < in.numberofpoints; ++i) {
          in.pointlist[i*2]=vInputPoints.at(i).x;
          in.pointlist[(i*2)+1]=vInputPoints.at(i).y;
          in.pointmarkerlist[i]=0;
      }
      in.numberofsegments = 0;
      in.numberofholes = 0;
      in.numberofregions = 0;

      mid.pointlist = (float *) NULL;            /* Not needed if -N switch used. */
      mid.pointmarkerlist = (int *) NULL; /* Not needed if -N or -B switch used. */
      mid.trianglelist = (int *) NULL;          /* Not needed if -E switch used. */

//      triangulate("za.2q32.5", &in, &mid, (struct triangulateio *) NULL);
}

void TriangleCProcess::readTerrain(std::vector<Point>& vInPoints,const char* filename)
{

    QFile inputFile(filename);
    if (inputFile.open(QIODevice::ReadOnly))
    {
       QTextStream in(&inputFile);
       while (!in.atEnd())
       {
          bool toRemove=false;
          QString line = in.readLine();
          QStringList splitedLine= line.split(" ");
          float x,y,z;
          x=splitedLine.at(0).toDouble();
          y=splitedLine.at(1).toDouble();

          Point p;
          vInPoints.push_back(p);
       }
       inputFile.close();

    }
}
