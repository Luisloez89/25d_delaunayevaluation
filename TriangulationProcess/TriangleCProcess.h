#ifndef TRIANGLECPROCESS_H
#define TRIANGLECPROCESS_H

#include <qstringlist.h>
#include "ProcessManager/ProcessConcurrent.h"
#include <QFileInfo>

struct Point{
    float x;
    float y;
};
class TriangleCProcess: public ProcessConcurrent
{
public:
    TriangleCProcess(QString inputFile,QStringList constraintFileList,QStringList exclusionFileList);
    ~TriangleCProcess();
    virtual void run();
private:
    QFileInfo *mInputFileInfo,*mOutputFileInfo;
    QStringList mConstraintFileList, mExclusionFileList;
    void readTerrain(std::vector<Point>& vInPoints,const char* filename);
};

#endif // TRIANGLECPROCESS_H
