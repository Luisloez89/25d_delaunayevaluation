#include "TrianglePlusPlusProcess.h"
#include <QDir>
#include <QFileInfo>

TrianglePlusPlusProcess::TrianglePlusPlusProcess(QString inputFile):
    ExternalProcess(QDir::currentPath()+"/ExternalBinaries/TrianglePP.exe")
{
    QFileInfo inputFileInfo(inputFile);
    QFileInfo outputFileInfo(inputFileInfo.absolutePath()+"/"+inputFileInfo.baseName()+"_TrianglePlusPlusResult.obj");

    setWorkingDir(inputFileInfo.absolutePath());

    setStartupMessage("Triangulating...");


    QStringList inputs;

    inputs << inputFileInfo.absoluteFilePath() <<  outputFileInfo.absoluteFilePath();

    addIntputs(inputs);
}

TrianglePlusPlusProcess::~TrianglePlusPlusProcess()
{

}

