#ifndef TRIANGLEPLUSPLUSPROCESS_H
#define TRIANGLEPLUSPLUSPROCESS_H

#include "ProcessManager/ExternalProcess.h"

class TrianglePlusPlusProcess: public ExternalProcess
{
public:
    TrianglePlusPlusProcess(QString inputFile);
    ~TrianglePlusPlusProcess();
};

#endif // TRIANGLEPLUSPLUSPROCESS_H
