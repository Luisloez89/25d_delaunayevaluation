#include "Triangle_PP_Process.h"
#include <QMessageBox>


Triangle_PP_Process::Triangle_PP_Process(QString inputFile,QStringList constraintFileList,QStringList exclusionFileList):
    mConstraintFileList(constraintFileList),
    mExclusionFileList(exclusionFileList)
{
    mInputFileInfo = new QFileInfo(inputFile);
    mOutputFileInfo = new QFileInfo(mInputFileInfo->absolutePath()+"/"+mInputFileInfo->baseName()+"_TriangleppResult.obj");
    setStartupMessage("Triangulating...");
}

Triangle_PP_Process::~Triangle_PP_Process()
{

}

void Triangle_PP_Process::run(){

    //Remove previous results
    if (mOutputFileInfo->exists()) {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Remove previous results");
        msgBox.setIconPixmap(QPixmap(":/img/trianglIcon.png"));
        msgBox.setText("Current results will be removed.");
        msgBox.setInformativeText("Do you want to continue?");
        msgBox.setStandardButtons(QMessageBox::Cancel | QMessageBox::Ok);
        msgBox.setDefaultButton(QMessageBox::Ok);
        int ret = msgBox.exec();
        if (ret!= QMessageBox::Ok) {
            return;
        }
        QFile(mOutputFileInfo->absoluteFilePath()).remove();
    }
    std::vector<Delaunay::Point> vInputPoints;
    readTerrain(vInputPoints,mInputFileInfo->absoluteFilePath().toAscii().constData());
    // 1. standard triangulation
    Delaunay trGenerator(vInputPoints);
    trGenerator.Triangulate();

    // iterate over triangles
        for (Delaunay::fIterator fit = trGenerator.fbegin(); fit != trGenerator.fend(); ++fit)
        {
            int keypointIdx1 = trGenerator.Org(fit);
            int keypointIdx2 = trGenerator.Dest(fit);
            int keypointIdx3 = trGenerator.Apex(fit);

            // access data
            double x1 = vInputPoints[keypointIdx1][0];
            double y1 = vInputPoints[keypointIdx1][1];
        }
}

void Triangle_PP_Process::readTerrain(std::vector<Delaunay::Point>& vInPoints,const char* filename)
{

    QFile inputFile(filename);
    if (inputFile.open(QIODevice::ReadOnly))
    {
       QTextStream in(&inputFile);
       while (!in.atEnd())
       {
          bool toRemove=false;
          QString line = in.readLine();
          QStringList splitedLine= line.split(" ");
          float x,y,z;
          x=splitedLine.at(0).toDouble();
          y=splitedLine.at(1).toDouble();

          Delaunay::Point p(x,y);
          vInPoints.push_back(p);
       }
       inputFile.close();

    }
}
