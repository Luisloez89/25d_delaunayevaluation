#ifndef TRIANGLE_PP_PROCESS_H
#define TRIANGLE_PP_PROCESS_H
#include <qstringlist.h>
#include "ProcessManager/ProcessConcurrent.h"
#include <QFileInfo>
#include "TrianglePP/tpp_interface.hpp"

using namespace tpp;

class Triangle_PP_Process: public ProcessConcurrent
{
public:
    Triangle_PP_Process(QString inputFile,QStringList constraintFileList,QStringList exclusionFileList);
    ~Triangle_PP_Process();
    virtual void run();
private:
    QFileInfo *mInputFileInfo,*mOutputFileInfo;
    QStringList mConstraintFileList, mExclusionFileList;
    void readTerrain(std::vector<Delaunay::Point>& vInPoints,const char* filename);
};

#endif // TRIANGLE_PP_PROCESS_H
