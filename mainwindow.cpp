#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "ccHObject.h"
#include "ccPointCloud.h"
#include "ccGLWindow.h"
#include "qCC_db_dll.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QLabel>
#include "TriangulationProcess/CGALTriang.h"
#include "TriangulationProcess/GDel3D_GPU_Process.h"
#include "Las2txtProcess.h"
#include "AboutDelaunayEvaluator.h"
#include <QDesktopWidget>
#include <QFile>
#include <QTextStream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);

    ui->setupUi(this);
    setWindowTitle("DelaunayEvaluation");
    setWindowIcon(QIcon(":/img/trianglIcon.png"));
    mainTabWidget = new QTabWidget();
    mainTabWidget->setStyleSheet("background-color:transparent");
    mCCVWDataSet = new ccViewerWrapper();
    mainTabWidget->addTab((QWidget*)mCCVWDataSet->getGLWindow(),tr("Source data"));
    mCCVWResults = new ccViewerWrapper();
    mainTabWidget->addTab((QWidget*)mCCVWResults->getGLWindow(),tr("Results"));


    mainTabWidget->setTabEnabled(1, false);

    setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    setCentralWidget(mainTabWidget);

    mDockResults = new QDockWidget(tr("Results"),this);
    mDockResults->setFeatures ( QDockWidget::NoDockWidgetFeatures );
    addDockWidget(Qt::BottomDockWidgetArea, mDockResults);
    mDockSettings = new QDockWidget(tr("Settings"),this);
    mDockSettings->setFeatures ( QDockWidget::NoDockWidgetFeatures );
    addDockWidget(Qt::LeftDockWidgetArea, mDockSettings);

    QFrame *settingsFrame = new QFrame();
    QVBoxLayout *settingsFrameLayout = new QVBoxLayout();

    QFrame *toolSelectorFrame = new QFrame();
    QHBoxLayout *toolSelectorFrameLayout = new QHBoxLayout();
    toolSelectorFrameLayout->setContentsMargins(0,0,0,0);
    toolSelectorFrameLayout->addWidget(new QLabel("Triangulation tool"));
    mCbTool = new QComboBox();
    mCbTool->addItem("Lastools (las2tin)");
    mCbTool->addItem("FADE2.5D");
    mCbTool->addItem("CGAL_2.5D");
    mCbTool->addItem("TriangleC");
    mCbTool->addItem("gDel3D_GPU");
    mCbTool->addItem("PCL_3D");
    mCbTool->addItem("PCL_Poisson");




    connect(mCbTool,SIGNAL(currentIndexChanged(int)),this,SLOT(on_CbTool_selectionChanged(int)));
    toolSelectorFrameLayout->addWidget(mCbTool);

    toolSelectorFrame->setLayout(toolSelectorFrameLayout);

    settingsFrameLayout->addWidget(toolSelectorFrame);

    mClusterizeFrame = new QFrame();
    QHBoxLayout *mClusterizeFrameLayout = new QHBoxLayout();
    mClusterizeFrameLayout->setContentsMargins(0,0,0,0);
    mChClusterize = new QCheckBox("Clusterize input:  ");
    mClusterizeFrameLayout->addWidget(mChClusterize);
    mClusterizeFrameLayout->addWidget(new QLabel("Edge size: "));
    mClusterEdge = new QSpinBox();
    mClusterEdge->setRange(10,10000000);
    mClusterEdge->setSingleStep(10);
    mClusterEdge->setValue(100);
    mClusterEdge->setEnabled(false);
    mClusterizeFrameLayout->addWidget(mClusterEdge);
    connect(mChClusterize,SIGNAL(clicked(bool)),mClusterEdge,SLOT(setEnabled(bool)));
    mClusterizeFrame->setEnabled(false);

    mClusterizeFrame->setLayout(mClusterizeFrameLayout);
    settingsFrameLayout->addWidget(mClusterizeFrame);

    mChPreviewData = new QCheckBox("Preview input data");
    mChPreviewData->setChecked(true);
    mChPreviewData->setEnabled(true);
    settingsFrameLayout->addWidget(mChPreviewData);

    mChShowResults = new QCheckBox("Show results");
    mChShowResults->setChecked(true);
    mChShowResults->setEnabled(false);

    settingsFrameLayout->addWidget(mChShowResults);

    mChSaveResults = new QCheckBox("Save results");
    mChSaveResults->setChecked(true);
    mChSaveResults->setEnabled(false);

    settingsFrameLayout->addWidget(mChSaveResults);


    mPbAddConstraintLines = new QPushButton("Add constraint lines");
    connect(mPbAddConstraintLines,SIGNAL(clicked()),this,SLOT(on_PbAddConstraintLines_triggered()));
    settingsFrameLayout->addWidget(mPbAddConstraintLines);

    mPbAddExclusionArea = new QPushButton("Add exclusion area");
    connect(mPbAddExclusionArea,SIGNAL(clicked()),this,SLOT(on_PbAddExclusionArea_triggered()));
    settingsFrameLayout->addWidget(mPbAddExclusionArea);

    connect(mChClusterize,SIGNAL(clicked(bool)),mPbAddConstraintLines,SLOT(setDisabled(bool)));
    connect(mChClusterize,SIGNAL(clicked(bool)),mPbAddExclusionArea,SLOT(setDisabled(bool)));

    mPbAddConstraintLines->setEnabled(false);
    mPbAddExclusionArea->setEnabled(false);
    //Fade2.5D settings
    mGbFade25DSettings = new QGroupBox("Fade2.5D Settings");


    mRbKeepDelaunay = new QRadioButton(tr("Force Delaunay on constraints"));
    mRbIgnoreDelaunay = new QRadioButton(tr("Ignore Delaunay on constraints"));

    mRbIgnoreDelaunay->setChecked(true);
    QVBoxLayout *fade25DSettingsLayout = new QVBoxLayout;
    fade25DSettingsLayout->addWidget(mRbKeepDelaunay);
    fade25DSettingsLayout->addWidget(mRbIgnoreDelaunay);
    mGbFade25DSettings->setLayout(fade25DSettingsLayout);
    settingsFrameLayout->addWidget(mGbFade25DSettings);

    mGbFade25DSettings->setVisible(false);

    //CGAL settings
    mGbCGALSettings = new QGroupBox("CGAL Settings");


    mChRefineDelaunay = new QCheckBox(tr("Refine delaunay (conforming)"));

    mChRefineDelaunay->setChecked(false);
    QVBoxLayout *CGALDSettingsLayout = new QVBoxLayout;
    CGALDSettingsLayout->addWidget(mChRefineDelaunay);
    mGbCGALSettings->setLayout(CGALDSettingsLayout);
    settingsFrameLayout->addWidget(mGbCGALSettings);

    mGbCGALSettings->setVisible(false);



    mPbCompute = new QPushButton("Compute triangulation");
    settingsFrameLayout->addWidget(mPbCompute);
    settingsFrameLayout->addStretch();
    settingsFrame->setLayout(settingsFrameLayout);


    mDockSettings->setWidget(settingsFrame);

    QFrame *resultsFrame = new QFrame();
    QHBoxLayout *resultsFrameLayout = new QHBoxLayout();

    //GB input data
    mInputData = new QGroupBox("Input data");
    QGridLayout *inputDataLayout = new QGridLayout();
    inputDataLayout->setAlignment(Qt::AlignTop);

    inputDataLayout->addWidget(new QLabel("Number of points: "),0,0);
    inputDataLayout->addWidget(lbNumPoints = new QLabel(""),0,1);

    mInputData->setLayout(inputDataLayout);

    //GB output data
    mResultData = new QGroupBox("Output data");
    QGridLayout *outputDataLayout = new QGridLayout();
    outputDataLayout->setAlignment(Qt::AlignTop);

    outputDataLayout->addWidget(new QLabel("Number of edges: "),0,0);
    outputDataLayout->addWidget(lbNumEdges = new QLabel(""),0,1);
    outputDataLayout->addWidget(new QLabel("Number of triangles: "),1,0);
    outputDataLayout->addWidget(lbNumTriangles = new QLabel(""),1,1);

    mResultData->setLayout(outputDataLayout);

    //GB Execution data
    mTimeResults = new QGroupBox("Execution data");
    QGridLayout *resultsDataLayout = new QGridLayout();
    resultsDataLayout->setAlignment(Qt::AlignTop);

    resultsDataLayout->addWidget(new QLabel("Read time: "),0,0);
    resultsDataLayout->addWidget(lbReadTime = new QLabel(""),0,1);
    resultsDataLayout->addWidget(new QLabel("Clusterization time: "),1,0);
    resultsDataLayout->addWidget(lbClusteringTime = new QLabel(""),1,1);
    resultsDataLayout->addWidget(new QLabel("Triangulation time: "),2,0);
    resultsDataLayout->addWidget(lbTrianglTime = new QLabel(""),2,1);
    resultsDataLayout->addWidget(new QLabel("Write time: "),3,0);
    resultsDataLayout->addWidget(lbWriteTime = new QLabel(""),3,1);
    resultsDataLayout->addWidget(new QLabel("Total time: "),4,0);
    resultsDataLayout->addWidget(lbTotalTime = new QLabel(""),4,1);

    mTimeResults->setLayout(resultsDataLayout);

    resultsFrameLayout->addWidget(mInputData);
    resultsFrameLayout->addWidget(mResultData);
    resultsFrameLayout->addWidget(mTimeResults);

    QLabel *DelaunayEvaluationLogo = new QLabel();

    DelaunayEvaluationLogo->setPixmap(QPixmap(":/img/DelaunayEvaluationImage2.png").scaledToHeight(QApplication::desktop()->screenGeometry().height()/6,Qt::SmoothTransformation));
    resultsFrameLayout->addWidget(DelaunayEvaluationLogo);

    resultsFrame->setLayout(resultsFrameLayout);
    mDockResults->setWidget(resultsFrame);

    mProgressDialog = new ProgressDialog(this);
    mConsole = new QTextEdit(this);
    QFont* font = new QFont("Courier");
    font->setPixelSize(10);
    mConsole->setFont(*font);
    mConsole->setReadOnly(true);
    mProgressDialog->setConsole(mConsole);

    connect(mPbCompute,SIGNAL(clicked()),this,SLOT(on_pbCompute_triggered()));
    mPbCompute->setEnabled(false);
    showMaximized();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionExit_triggered()
{
    this->close();
}

void MainWindow::on_actionLoad_data_triggered()
{
    QString filters("ASCII file (*.txt);;Las file (*.las)");
    QString defaultFilter ="Las file (*.las)";

    mSourceFile = QFileDialog::getOpenFileName(this, tr("Open File"), "C://",filters,&defaultFilter);
    if (!mSourceFile.isEmpty()) {
        if (mChPreviewData->isChecked()) {
            mCCVWDataSet->addToDB(QStringList() << mSourceFile);
            mCCVWDataSet->activatePointDisplay();
        }
        mPbCompute->setEnabled(true);

        if (mCbTool->currentIndex()!= 5 && mCbTool->currentIndex()!= 6 && mCbTool->currentIndex()!= 7) {
            mClusterizeFrame->setEnabled(true);
        }

        mChShowResults->setEnabled(true);
        mChSaveResults->setEnabled(true);

        if (mCbTool->currentIndex()!=0 && !mChClusterize->isChecked()) {
            mPbAddConstraintLines->setEnabled(true);
            mPbAddExclusionArea->setEnabled(true);
        }
    }else {
        mPbCompute->setEnabled(false);
        mPbAddConstraintLines->setEnabled(false);
        mPbAddExclusionArea->setEnabled(false);
        mClusterizeFrame->setEnabled(false);

    }
}
void MainWindow::on_pbCompute_triggered(){

    mTime = new QTime();
    mTime->restart();

    lbNumEdges->setText("");
    lbNumPoints->setText("");
    lbNumTriangles->setText("");
    lbReadTime->setText("");
    lbTrianglTime->setText("");
    lbWriteTime->setText("");
    lbClusteringTime->setText("");
    lbTotalTime->setText("");

    mReadMillis=0;
    mWriteMillis=0;
    mTriangMillis=0;

    mPointsCount=0;

    QFileInfo inputFileInfo(mSourceFile);
    QFileInfo outputFileInfo;

    if (mChClusterize->isChecked()) {
        QDir resultsDir(inputFileInfo.absolutePath()+"/"+inputFileInfo.baseName()+"_ClusteringResults");

        if (resultsDir.exists()) {
            QMessageBox msgBox(this);
            msgBox.setWindowTitle("Remove previous results");
            msgBox.setText("Current results will be removed.");
            msgBox.setInformativeText("Do you want to continue?");
            msgBox.setStandardButtons(QMessageBox::Cancel | QMessageBox::Ok);
            msgBox.setDefaultButton(QMessageBox::Ok);
            int ret = msgBox.exec();
            if (ret!= QMessageBox::Ok) {
                return;
            }
            removeDir(resultsDir.absolutePath());

        }
        QDir().mkdir(resultsDir.absolutePath());

        mclusteringProcess= new ClusteringProcess(mSourceFile,mClusterEdge->value());


        connect(mclusteringProcess, SIGNAL(newStdData(QString)),this,SLOT(manageProccesStdOutput(QString)));
        connect(mclusteringProcess, SIGNAL(newErrorData(QString)),this,SLOT(manageProccesErrorOutput(QString)));
        connect(mclusteringProcess, SIGNAL(finished()),this,SLOT(on_ClusteringProcess_finished()));

        connect(mclusteringProcess, SIGNAL(error(int,QString)),this,SLOT(onError(int,QString)));
        connect(mProgressDialog,SIGNAL(cancel()),mclusteringProcess,SLOT(stop()));

        mConsole->clear();
        mProgressDialog->setModal(true);
        mProgressDialog->setRange(0,0);
        mProgressDialog->setWindowTitle("clustering...");
        mProgressDialog->setStatusText("clustering...");
        mProgressDialog->setFinished(false);
        mProgressDialog->show();
        mTime->restart();
        mclusteringProcess->start();
        return;
    }

    lbClusteringTime->setText("---");

    switch (mCbTool->currentIndex()) {
    case 0:

             outputFileInfo.setFile(inputFileInfo.absolutePath()+"/"+inputFileInfo.baseName()+"_las2tinResult.obj");
             //Remove previous results
             if (outputFileInfo.exists()) {
                 QMessageBox msgBox(this);
                 msgBox.setWindowTitle("Remove previous results");
                 msgBox.setText("Current results will be removed.");
                 msgBox.setInformativeText("Do you want to continue?");
                 msgBox.setStandardButtons(QMessageBox::Cancel | QMessageBox::Ok);
                 msgBox.setDefaultButton(QMessageBox::Ok);
                 int ret = msgBox.exec();
                 if (ret!= QMessageBox::Ok) {
                     return;
                 }
                 QFile(outputFileInfo.absoluteFilePath()).remove();
             }

             mLas2tinProcess = new Las2tinProcess(mSourceFile);
             connect(mLas2tinProcess, SIGNAL(newStdData(QString)),this,SLOT(manageProccesStdOutput(QString)));
             connect(mLas2tinProcess, SIGNAL(newErrorData(QString)),this,SLOT(manageProccesErrorOutput(QString)));
             connect(mLas2tinProcess, SIGNAL(finished()),this,SLOT(on_las2tinProcess_finished()));

             connect(mLas2tinProcess, SIGNAL(error(int,QString)),this,SLOT(onError(int,QString)));
             connect(mProgressDialog,SIGNAL(cancel()),mLas2tinProcess,SLOT(stop()));

             mConsole->clear();
             mProgressDialog->setModal(true);
             mProgressDialog->setRange(0,0);
             mProgressDialog->setWindowTitle("LasTools - Triangulating point cloud...");
             mProgressDialog->setStatusText("LasTools - Triangulating point cloud...");
             mProgressDialog->setFinished(false);
             mProgressDialog->show();
             mTime->restart();
             mLas2tinProcess->start();
        break;
    case 1:


            outputFileInfo.setFile(inputFileInfo.absolutePath()+"/"+inputFileInfo.baseName()+"_Fade25DResult.obj");
            //Remove previous results

            if (outputFileInfo.exists()) {
                QMessageBox msgBox;
                msgBox.setWindowTitle("Remove previous results");
                msgBox.setIconPixmap(QPixmap(":/img/trianglIcon.png"));
                msgBox.setText("Current results will be removed.");
                msgBox.setInformativeText("Do you want to continue?");
                msgBox.setStandardButtons(QMessageBox::Cancel | QMessageBox::Ok);
                msgBox.setDefaultButton(QMessageBox::Ok);
                int ret = msgBox.exec();
                if (ret!= QMessageBox::Ok) {
                    return;
                }
                QFile(outputFileInfo.absoluteFilePath()).remove();
            }

            mFade25DProcess = new Fade25DProcess(mSourceFile,mConstraintFile,mExclusionAreaFile,mRbKeepDelaunay->isChecked(),
                                                 lbReadTime,lbTrianglTime,lbWriteTime,lbNumPoints,lbNumTriangles,lbNumEdges,mChClusterize->isChecked());
            connect(mFade25DProcess, SIGNAL(newStdData(QString)),this,SLOT(manageProccesStdOutput(QString)));
            connect(mFade25DProcess, SIGNAL(newErrorData(QString)),this,SLOT(manageProccesErrorOutput(QString)));
            connect(mFade25DProcess, SIGNAL(finished()),this,SLOT(on_Fade25DProcess_finished()));

            connect(mFade25DProcess, SIGNAL(error(int,QString)),this,SLOT(onError(int,QString)));
            connect(mProgressDialog,SIGNAL(cancel()),mFade25DProcess,SLOT(stop()));

            mConsole->clear();
            mProgressDialog->setModal(true);
            mProgressDialog->setRange(0,0);
            mProgressDialog->setWindowTitle("Fade 2.5D - Triangulating point cloud...");
            mProgressDialog->setStatusText("Fade 2.5D - Triangulating point cloud...");
            mProgressDialog->setFinished(false);
            mProgressDialog->show();
            mFade25DProcess->start();


        break;
    case 2:
        outputFileInfo.setFile(inputFileInfo.absolutePath()+"/"+inputFileInfo.baseName()+"_CGALResult.obj");
        if (outputFileInfo.exists()) {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Remove previous results");
            msgBox.setIconPixmap(QPixmap(":/img/trianglIcon.png"));
            msgBox.setText("Current results will be removed.");
            msgBox.setInformativeText("Do you want to continue?");
            msgBox.setStandardButtons(QMessageBox::Cancel | QMessageBox::Ok);
            msgBox.setDefaultButton(QMessageBox::Ok);
            int ret = msgBox.exec();
            if (ret!= QMessageBox::Ok) {
                return;
            }
            QFile(outputFileInfo.absoluteFilePath()).remove();
        }
        mCGALProcess = new CGALTriang(mSourceFile,mConstraintFile,mExclusionAreaFile,mChRefineDelaunay->isChecked(),
                                      lbReadTime,lbTrianglTime,lbWriteTime,lbNumPoints,lbNumTriangles,lbNumEdges,mChSaveResults->isChecked(),mChClusterize->isChecked());

        connect(mCGALProcess, SIGNAL(newStdData(QString)),this,SLOT(manageProccesStdOutput(QString)));
        connect(mCGALProcess, SIGNAL(newErrorData(QString)),this,SLOT(manageProccesErrorOutput(QString)));
        connect(mCGALProcess, SIGNAL(finished()),this,SLOT(on_CGALProcess_finished()));

        connect(mCGALProcess, SIGNAL(error(int,QString)),this,SLOT(onError(int,QString)));
        connect(mProgressDialog,SIGNAL(cancel()),mCGALProcess,SLOT(stop()));

        mConsole->clear();
        mProgressDialog->setModal(true);
        mProgressDialog->setRange(0,0);
        mProgressDialog->setWindowTitle("CGAL - Triangulating point cloud...");
        mProgressDialog->setStatusText("CGAL - Triangulating point cloud...");
        mProgressDialog->setFinished(false);
        mProgressDialog->show();
        mCGALProcess->start();
        break;
    case 3:
        outputFileInfo.setFile(inputFileInfo.absolutePath()+"/"+inputFileInfo.baseName()+"_TriangleCCResult.obj");
        if (outputFileInfo.exists()) {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Remove previous results");
            msgBox.setIconPixmap(QPixmap(":/img/trianglIcon.png"));
            msgBox.setText("Current results will be removed.");
            msgBox.setInformativeText("Do you want to continue?");
            msgBox.setStandardButtons(QMessageBox::Cancel | QMessageBox::Ok);
            msgBox.setDefaultButton(QMessageBox::Ok);
            int ret = msgBox.exec();
            if (ret!= QMessageBox::Ok) {
                return;
            }
            QFile(outputFileInfo.absoluteFilePath()).remove();
        }
        mTriangleCCProcess = new TriangleCCProcess(mSourceFile,
                                                   lbReadTime,lbTrianglTime,lbWriteTime,lbNumPoints,lbNumTriangles,lbNumEdges,mChClusterize->isChecked());

        connect(mTriangleCCProcess, SIGNAL(newStdData(QString)),this,SLOT(manageProccesStdOutput(QString)));
        connect(mTriangleCCProcess, SIGNAL(newErrorData(QString)),this,SLOT(manageProccesErrorOutput(QString)));
        connect(mTriangleCCProcess, SIGNAL(finished()),this,SLOT(on_Triangle_CC_Process_finished()));

        connect(mTriangleCCProcess, SIGNAL(error(int,QString)),this,SLOT(onError(int,QString)));
        connect(mProgressDialog,SIGNAL(cancel()),mTriangleCCProcess,SLOT(stop()));

        mConsole->clear();
        mProgressDialog->setModal(true);
        mProgressDialog->setRange(0,0);
        mProgressDialog->setWindowTitle("TriangleC - Triangulating point cloud...");
        mProgressDialog->setStatusText("TriangleC - Triangulating point cloud...");
        mProgressDialog->setFinished(false);
        mProgressDialog->show();
        mTriangleCCProcess->start();
        break;
    case 4:
        mGDel3DProcess = new MultiProcess(true);

        if (inputFileInfo.suffix().compare("las",Qt::CaseInsensitive)==0) {
            mGDel3DProcess->appendProcess(new Las2txtProcess(mSourceFile));
        }

        mGDel3DProcess->appendProcess(new GDel3D_GPU_Process(mSourceFile));

        connect(mGDel3DProcess, SIGNAL(newStdData(QString)),this,SLOT(manageProccesStdOutput(QString)));
        connect(mGDel3DProcess, SIGNAL(newErrorData(QString)),this,SLOT(manageProccesErrorOutput(QString)));
        connect(mGDel3DProcess, SIGNAL(finished()),this,SLOT(on_gDel3D_Process_finished()));

        connect(mGDel3DProcess, SIGNAL(error(int,QString)),this,SLOT(onError(int,QString)));
        connect(mProgressDialog,SIGNAL(cancel()),mGDel3DProcess,SLOT(stop()));

        mConsole->clear();
        mProgressDialog->setModal(true);
        mProgressDialog->setRange(0,0);
        mProgressDialog->setWindowTitle("gDel3D GPU - Triangulating point cloud...");
        mProgressDialog->setStatusText("gDel3D GPU - Triangulating point cloud...");
        mProgressDialog->setFinished(false);
        mProgressDialog->show();
        mTime->restart();
        mGDel3DProcess->start();
        break;
    case 5:
        outputFileInfo.setFile(inputFileInfo.absolutePath()+"/"+inputFileInfo.baseName()+"_PCL3DResult.obj");
        if (outputFileInfo.exists()) {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Remove previous results");
            msgBox.setIconPixmap(QPixmap(":/img/trianglIcon.png"));
            msgBox.setText("Current results will be removed.");
            msgBox.setInformativeText("Do you want to continue?");
            msgBox.setStandardButtons(QMessageBox::Cancel | QMessageBox::Ok);
            msgBox.setDefaultButton(QMessageBox::Ok);
            int ret = msgBox.exec();
            if (ret!= QMessageBox::Ok) {
                return;
            }
            QFile(outputFileInfo.absoluteFilePath()).remove();
        }
        mPCL3DProcess = new PCLTriangl3DProcess(mSourceFile, lbReadTime,lbTrianglTime,lbWriteTime,lbNumPoints,lbNumTriangles,lbNumEdges,mChSaveResults->isChecked());

        connect(mPCL3DProcess, SIGNAL(newStdData(QString)),this,SLOT(manageProccesStdOutput(QString)));
        connect(mPCL3DProcess, SIGNAL(newErrorData(QString)),this,SLOT(manageProccesErrorOutput(QString)));
        connect(mPCL3DProcess, SIGNAL(finished()),this,SLOT(on_PCL3DProcess_finished()));

        connect(mPCL3DProcess, SIGNAL(error(int,QString)),this,SLOT(onError(int,QString)));
        connect(mProgressDialog,SIGNAL(cancel()),mPCL3DProcess,SLOT(stop()));

        mConsole->clear();
        mProgressDialog->setModal(true);
        mProgressDialog->setRange(0,0);
        mProgressDialog->setWindowTitle("PCL 3D - Triangulating point cloud...");
        mProgressDialog->setStatusText("PCL 3D - Triangulating point cloud...");
        mProgressDialog->setFinished(false);
        mProgressDialog->show();
        mPCL3DProcess->start();
        break;
    case 6:
        outputFileInfo.setFile(inputFileInfo.absolutePath()+"/"+inputFileInfo.baseName()+"_PCLPoissonResult.obj");
        if (outputFileInfo.exists()) {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Remove previous results");
            msgBox.setIconPixmap(QPixmap(":/img/trianglIcon.png"));
            msgBox.setText("Current results will be removed.");
            msgBox.setInformativeText("Do you want to continue?");
            msgBox.setStandardButtons(QMessageBox::Cancel | QMessageBox::Ok);
            msgBox.setDefaultButton(QMessageBox::Ok);
            int ret = msgBox.exec();
            if (ret!= QMessageBox::Ok) {
                return;
            }
            QFile(outputFileInfo.absoluteFilePath()).remove();
        }
        mPCLPoissonProcess  = new PCLPoissonProcess(mSourceFile, lbReadTime,lbTrianglTime,lbWriteTime,lbNumPoints,lbNumTriangles,lbNumEdges,mChSaveResults->isChecked());

        connect(mPCLPoissonProcess, SIGNAL(newStdData(QString)),this,SLOT(manageProccesStdOutput(QString)));
        connect(mPCLPoissonProcess, SIGNAL(newErrorData(QString)),this,SLOT(manageProccesErrorOutput(QString)));
        connect(mPCLPoissonProcess, SIGNAL(finished()),this,SLOT(on_PCLPoissonProcess_finished()));

        connect(mPCLPoissonProcess, SIGNAL(error(int,QString)),this,SLOT(onError(int,QString)));
        connect(mProgressDialog,SIGNAL(cancel()),mPCLPoissonProcess,SLOT(stop()));

        mConsole->clear();
        mProgressDialog->setModal(true);
        mProgressDialog->setRange(0,0);
        mProgressDialog->setWindowTitle("PCL Poisson - Triangulating point cloud...");
        mProgressDialog->setStatusText("PCL Poisson - Triangulating point cloud...");
        mProgressDialog->setFinished(false);
        mProgressDialog->show();
        mPCLPoissonProcess->start();
        break;
    }
}

void MainWindow::manageProccesStdOutput(QString data)
{
    mConsole->append("<font color='black'>" + data + "\n</font>");
}

void MainWindow::manageProccesErrorOutput(QString data)
{
    mConsole->append("<font color='#999900'>" + data + "</font>");
}

void MainWindow::onError(int code, QString cause)
{

}

void MainWindow::on_las2tinProcess_finished(){
    if (!mLas2tinProcess->isStopped()) {
        QFileInfo inputFileInfo(mSourceFile);
        QFileInfo outputFileInfo(inputFileInfo.absolutePath()+"/"+inputFileInfo.baseName()+"_las2tinResult.obj");

        mProgressDialog->setRange(0,1);
        mProgressDialog->setValue(1);
        mProgressDialog->setFinished(true);

        if (outputFileInfo.exists()) {
            mProgressDialog->setStatusText(tr("Process finished."));
            int millis, hours, min, seg;
            millis = mTime->elapsed();
            hours = millis/3600000;
            min = (millis-(3600000*hours))/60000;
            seg = (millis-(3600000*hours)-(60000*min))/1000;

            QFileInfo resultsFile(QFileInfo(mSourceFile).absolutePath()+"/"+"Time_Las2tin.txt");
            QFile *timeResultsfile= new QFile(resultsFile.absoluteFilePath());
            if (timeResultsfile->open(QIODevice::ReadWrite|QIODevice::Append)) {
                QTextStream stream(timeResultsfile);
                stream <<QString::number(millis) << endl;
                timeResultsfile->close();
            }

            lbTotalTime->setText(QString::number(hours)+" h "+QString::number(min)+" min "+QString::number(seg)+" seg");
            if (mChShowResults->isChecked()) {
                mCCVWResults->addToDB(QStringList() << outputFileInfo.absoluteFilePath());
                mainTabWidget->setTabEnabled(1, true);
                mainTabWidget->setCurrentIndex(1);
            }else {
                QMessageBox msgBox(this);
                msgBox.setWindowTitle("Results location");
                msgBox.setText("Current results are located on the path:");
                msgBox.setInformativeText(outputFileInfo.absoluteFilePath());
                msgBox.exec();
            }
        }else {
            mProgressDialog->setStatusText(tr("Process failed."));

        }
    }
    disconnect(mProgressDialog,SIGNAL(cancel()),mLas2tinProcess,SLOT(stop()));

}

void MainWindow::on_Fade25DProcess_finished(){
    if (!mFade25DProcess->isStopped()) {
        QFileInfo inputFileInfo(mSourceFile);
        QFileInfo outputFileInfo(inputFileInfo.absolutePath()+"/"+inputFileInfo.baseName()+"_Fade25DResult.obj");

        mProgressDialog->setRange(0,1);
        mProgressDialog->setValue(1);
        mProgressDialog->setFinished(true);

        if (outputFileInfo.exists()) {
            mProgressDialog->setStatusText(tr("Process finished."));
            int millis, hours, min, seg;
            millis = mTime->elapsed();
            hours = millis/3600000;
            min = (millis-(3600000*hours))/60000;
            seg = (millis-(3600000*hours)-(60000*min))/1000;
            lbTotalTime->setText(QString::number(hours)+" h "+QString::number(min)+" min "+QString::number(seg)+" seg");

            if (mChShowResults->isChecked()) {
                mCCVWResults->addToDB(QStringList() << outputFileInfo.absoluteFilePath());

                mainTabWidget->setTabEnabled(1, true);
                mainTabWidget->setCurrentIndex(1);
            }else {
                QMessageBox msgBox(this);
                msgBox.setWindowTitle("Results location");
                msgBox.setText("Current results are located on the path:");
                msgBox.setInformativeText(outputFileInfo.absoluteFilePath());
                msgBox.exec();
            }

        }else {
            mProgressDialog->setStatusText(tr("Process failed."));
        }

    }
    disconnect(mProgressDialog,SIGNAL(cancel()),mFade25DProcess,SLOT(stop()));
}

void MainWindow::on_PbAddConstraintLines_triggered(){
    QString filters("ASCII file (*.txt)");
    QString defaultFilter ="ASCII file (*.txt)";

    mConstraintFile = QFileDialog::getOpenFileName(this, tr("Open File"), "C://",filters,&defaultFilter);
}

void MainWindow::on_PbAddExclusionArea_triggered(){
    QString filters("ASCII file (*.txt)");
    QString defaultFilter ="ASCII file (*.txt)";

    mExclusionAreaFile = QFileDialog::getOpenFileName(this, tr("Open File"), "C://",filters,&defaultFilter);
}

void MainWindow::on_CbTool_selectionChanged(int currentIndex){

    if (mCbTool->currentIndex()== 5 || mCbTool->currentIndex()== 6 || mCbTool->currentIndex()== 7) {
        mChClusterize->setChecked(false);
        mChClusterize->setEnabled(false);
        mClusterEdge->setEnabled(false);
    }else {
        mChClusterize->setEnabled(true);
        mClusterEdge->setEnabled(true);

    }
    switch (currentIndex) {
    case 1:
        mGbFade25DSettings->setVisible(true);
        mGbCGALSettings->setVisible(false);
        if (!mChClusterize->isChecked()) {
            mPbAddConstraintLines->setEnabled(true);
            mPbAddExclusionArea->setEnabled(true);
        }
        break;
    case 2:
            mGbFade25DSettings->setVisible(false);
            mGbCGALSettings->setVisible(true);
            if (!mChClusterize->isChecked()) {
                mPbAddConstraintLines->setEnabled(true);
                mPbAddExclusionArea->setEnabled(true);
            }
        break;
    default:
        mGbFade25DSettings->setVisible(false);
        mGbCGALSettings->setVisible(false);
        mPbAddConstraintLines->setEnabled(false);
        mPbAddExclusionArea->setEnabled(false);
        break;
    }
}

void MainWindow::on_CGALProcess_finished(){
    if (!mCGALProcess->isStopped()) {
        QFileInfo inputFileInfo(mSourceFile);
        QFileInfo outputFileInfo(inputFileInfo.absolutePath()+"/"+inputFileInfo.baseName()+"_CGALResult.obj");

        mProgressDialog->setRange(0,1);
        mProgressDialog->setValue(1);
        mProgressDialog->setFinished(true);

        if (outputFileInfo.exists() || !mChSaveResults->isChecked()) {
            mProgressDialog->setStatusText(tr("Process finished."));
            int millis, hours, min, seg;
            millis = mTime->elapsed();
            hours = millis/3600000;
            min = (millis-(3600000*hours))/60000;
            seg = (millis-(3600000*hours)-(60000*min))/1000;
            lbTotalTime->setText(QString::number(hours)+" h "+QString::number(min)+" min "+QString::number(seg)+" seg");
            if (mChSaveResults->isChecked()) {
                if (mChShowResults->isChecked()) {
                    mCCVWResults->addToDB(QStringList() << outputFileInfo.absoluteFilePath());
                    mainTabWidget->setTabEnabled(1, true);
                    mainTabWidget->setCurrentIndex(1);
                }else {
                    QMessageBox msgBox(this);
                    msgBox.setWindowTitle("Results location");
                    msgBox.setText("Current results are located on the path:");
                    msgBox.setInformativeText(outputFileInfo.absoluteFilePath());
                    msgBox.exec();
                }
            }
        }else {
            mProgressDialog->setStatusText(tr("Process failed."));
        }

    }
    disconnect(mProgressDialog,SIGNAL(cancel()),mCGALProcess,SLOT(stop()));
}

void MainWindow::on_actionAbout_triggered()
{
    AboutDelaunayEvaluator *aboutDialog = new AboutDelaunayEvaluator(this);
    aboutDialog->exec();
}

void MainWindow::on_Triangle_PP_Process_finished(){
    if (!mTriangle_PP_Process->isStopped()) {
        QFileInfo inputFileInfo(mSourceFile);
        QFileInfo outputFileInfo(inputFileInfo.absolutePath()+"/"+inputFileInfo.baseName()+"_TrianglePlusPlusResult.obj");

        mProgressDialog->setRange(0,1);
        mProgressDialog->setValue(1);
        mProgressDialog->setFinished(true);

        if (outputFileInfo.exists()) {
            mProgressDialog->setStatusText(tr("Process finished."));
            int millis, hours, min, seg;
            millis = mTime->elapsed();
            hours = millis/3600000;
            min = (millis-(3600000*hours))/60000;
            seg = (millis-(3600000*hours)-(60000*min))/1000;
            lbTotalTime->setText(QString::number(hours)+" h "+QString::number(min)+" min "+QString::number(seg)+" seg");
            if (mChShowResults->isChecked()) {
                mCCVWResults->addToDB(QStringList() << outputFileInfo.absoluteFilePath());
                mainTabWidget->setTabEnabled(1, true);
                mainTabWidget->setCurrentIndex(1);
            }else {
                QMessageBox msgBox(this);
                msgBox.setWindowTitle("Results location");
                msgBox.setText("Current results are located on the path:");
                msgBox.setInformativeText(outputFileInfo.absoluteFilePath());
                msgBox.exec();
            }

        }else {
            mProgressDialog->setStatusText(tr("Process failed."));
        }

    }
    disconnect(mProgressDialog,SIGNAL(cancel()),mTriangle_PP_Process,SLOT(stop()));
}

void MainWindow::on_Triangle_CC_Process_finished(){
    if (!mTriangleCCProcess->isStopped()) {
        QFileInfo inputFileInfo(mSourceFile);
        QFileInfo outputFileInfo(inputFileInfo.absolutePath()+"/"+inputFileInfo.baseName()+"_TriangleCCResult.obj");

        mProgressDialog->setRange(0,1);
        mProgressDialog->setValue(1);
        mProgressDialog->setFinished(true);

        if (outputFileInfo.exists()) {
            mProgressDialog->setStatusText(tr("Process finished."));
            int millis, hours, min, seg;
            millis = mTime->elapsed();
            hours = millis/3600000;
            min = (millis-(3600000*hours))/60000;
            seg = (millis-(3600000*hours)-(60000*min))/1000;
            lbTotalTime->setText(QString::number(hours)+" h "+QString::number(min)+" min "+QString::number(seg)+" seg");
            if (mChShowResults->isChecked()) {
                mCCVWResults->addToDB(QStringList() << outputFileInfo.absoluteFilePath());
                mainTabWidget->setTabEnabled(1, true);
                mainTabWidget->setCurrentIndex(1);
            }else {
                QMessageBox msgBox(this);
                msgBox.setWindowTitle("Results location");
                msgBox.setText("Current results are located on the path:");
                msgBox.setInformativeText(outputFileInfo.absoluteFilePath());
                msgBox.exec();
            }

        }else {
            mProgressDialog->setStatusText(tr("Process failed."));
        }

    }
    disconnect(mProgressDialog,SIGNAL(cancel()),mTriangleCCProcess,SLOT(stop()));
}


void MainWindow::on_ClusteringProcess_finished(){
    if (!mclusteringProcess->isStopped()) {
        int millis, hours, min, seg;
        millis = mTime->elapsed();
        hours = millis/3600000;
        min = (millis-(3600000*hours))/60000;
        seg = (millis-(3600000*hours)-(60000*min))/1000;
        lbClusteringTime->setText(QString::number(hours)+" h "+QString::number(min)+" min "+QString::number(seg)+" seg");

        QFileInfo inputFileInfo(mSourceFile);
        QDir resultsDir(inputFileInfo.absolutePath()+"/"+inputFileInfo.baseName()+"_ClusteringResults");
        resultsDir.setFilter(QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks);

        mMultiprocessSourceList =resultsDir.entryList();
        if (mMultiprocessSourceList.count()<1) {
            return; //AQUI EVALUAR EL ERROR EN LA CLUSTERIZACION
        }
        mParallelTriangulationProcess = new MultiProcess(false);

        switch (mCbTool->currentIndex()) {
        case 0:
                 for (int i = 0; i < mMultiprocessSourceList.count(); ++i) {
                    QFileInfo fileToTriangulate(resultsDir.absolutePath()+"/"+mMultiprocessSourceList.at(i));
                     mParallelTriangulationProcess->appendProcess(new Las2tinProcess(fileToTriangulate.absoluteFilePath()));
                 }
            break;
        case 1:
            for (int i = 0; i < mMultiprocessSourceList.count(); ++i) {
               QFileInfo fileToTriangulate(resultsDir.absolutePath()+"/"+mMultiprocessSourceList.at(i));
                mParallelTriangulationProcess->appendProcess(new Fade25DProcess(fileToTriangulate.absoluteFilePath(),mConstraintFile,mExclusionAreaFile,mRbKeepDelaunay->isChecked(),
                                                                                lbReadTime,lbTrianglTime,lbWriteTime,lbNumPoints,lbNumTriangles,lbNumEdges,mChClusterize->isChecked()));
            }
            break;
        case 2:
            for (int i = 0; i < mMultiprocessSourceList.count(); ++i) {
               QFileInfo fileToTriangulate(resultsDir.absolutePath()+"/"+mMultiprocessSourceList.at(i));
                mParallelTriangulationProcess->appendProcess(new CGALTriang(fileToTriangulate.absoluteFilePath(),mConstraintFile,mExclusionAreaFile,mChRefineDelaunay->isChecked(),
                                                                            lbReadTime,lbTrianglTime,lbWriteTime,lbNumPoints,lbNumTriangles,lbNumEdges,mChSaveResults->isChecked(),mChClusterize->isChecked()));
            }
            break;
        case 3:
            for (int i = 0; i < mMultiprocessSourceList.count(); ++i) {
               QFileInfo fileToTriangulate(resultsDir.absolutePath()+"/"+mMultiprocessSourceList.at(i));
                mParallelTriangulationProcess->appendProcess(new TriangleCCProcess(fileToTriangulate.absoluteFilePath(),
                                                                                   lbReadTime,lbTrianglTime,lbWriteTime,lbNumPoints,lbNumTriangles,lbNumEdges,mChClusterize->isChecked()));
            }
            break;
        case 4:
            for (int i = 0; i < mMultiprocessSourceList.count(); ++i) {
               QFileInfo fileToTriangulate(resultsDir.absolutePath()+"/"+mMultiprocessSourceList.at(i));
                mParallelTriangulationProcess->appendProcess(new GDel3D_GPU_Process(fileToTriangulate.absoluteFilePath()));
            }
            break;
        }
        connect(mParallelTriangulationProcess, SIGNAL(newStdData(QString)),this,SLOT(manageProccesStdOutput(QString)));
        connect(mParallelTriangulationProcess, SIGNAL(newErrorData(QString)),this,SLOT(manageProccesErrorOutput(QString)));
        connect(mParallelTriangulationProcess, SIGNAL(finished()),this,SLOT(on_parallelTriangulationProcess_finished()));

        connect(mParallelTriangulationProcess, SIGNAL(error(int,QString)),this,SLOT(onError(int,QString)));
        connect(mProgressDialog,SIGNAL(cancel()),mParallelTriangulationProcess,SLOT(stop()));

        mConsole->clear();
        mProgressDialog->setModal(true);
        mProgressDialog->setRange(0,0);
        mProgressDialog->setWindowTitle("LasTools - Triangulating point cloud...");
        mProgressDialog->setStatusText("LasTools - Triangulating point cloud...");
        mProgressDialog->setFinished(false);
        mProgressDialog->show();
        mParallelTriangulationProcess->start();
    }
    disconnect(mProgressDialog,SIGNAL(cancel()),mclusteringProcess,SLOT(stop()));
}

bool MainWindow::removeDir(QString dirName)
{
    bool result = true;
    QDir dir(dirName);
    if (dir.exists(dirName)) {
        Q_FOREACH(QFileInfo info, dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files, QDir::DirsFirst)) {
            if (info.isDir()) {
                result = removeDir(info.absoluteFilePath());
            }
            else {
                result = QFile::remove(info.absoluteFilePath());
            }
            if (!result) {
                return result;
            }
        }
        result = dir.rmdir(dirName);
    }
    return result;
}

void MainWindow::on_parallelTriangulationProcess_finished(){
    if (!mParallelTriangulationProcess->isStopped()) {
        QFileInfo inputFileInfo(mSourceFile);
        QDir triangulationResultsDir(inputFileInfo.absolutePath()+"/"+inputFileInfo.baseName()+"_ClusteringResults");
        triangulationResultsDir.setNameFilters(QStringList("*.obj"));
        triangulationResultsDir.setFilter(QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks);

        QStringList results =triangulationResultsDir.entryList();
        QStringList resultsFullPath;
        foreach (QString res, results) {
            resultsFullPath.append(triangulationResultsDir.absoluteFilePath(res));
        }
        if (mMultiprocessSourceList.count()<1) {
            disconnect(mProgressDialog,SIGNAL(cancel()),mParallelTriangulationProcess,SLOT(stop()));
            mProgressDialog->setStatusText(tr("Process failed."));
            return;
        }

        mProgressDialog->setRange(0,1);
        mProgressDialog->setValue(1);
        mProgressDialog->setFinished(true);

        mProgressDialog->setStatusText(tr("Process finished."));

        int millis, hours, min, seg;
        millis = mTime->elapsed();
        hours = millis/3600000;
        min = (millis-(3600000*hours))/60000;
        seg = (millis-(3600000*hours)-(60000*min))/1000;

        QFileInfo resultsFile(QFileInfo(mSourceFile).absolutePath()+"/"+"Time_Clusterized.txt");
        QFile *timeResultsfile= new QFile(resultsFile.absoluteFilePath());
        if (timeResultsfile->open(QIODevice::ReadWrite|QIODevice::Append)) {
            QTextStream stream(timeResultsfile);
            stream <<QString::number(millis) << endl;
            timeResultsfile->close();
        }

        lbTotalTime->setText(QString::number(hours)+" h "+QString::number(min)+" min "+QString::number(seg)+" seg");

        if (mChShowResults->isChecked()) {
            mCCVWResults->addToDB(QStringList() << resultsFullPath);
            mainTabWidget->setTabEnabled(1, true);
            mainTabWidget->setCurrentIndex(1);
        }else {
            QMessageBox msgBox(this);
            msgBox.setWindowTitle("Results location");
            msgBox.setText("Current results are located on the path:");
            msgBox.setInformativeText(triangulationResultsDir.absolutePath());
            msgBox.exec();
        }

    }
    disconnect(mProgressDialog,SIGNAL(cancel()),mParallelTriangulationProcess,SLOT(stop()));
}


void MainWindow::on_gDel3D_Process_finished(){
    if (!mGDel3DProcess->isStopped()) {
        mProgressDialog->setRange(0,1);
        mProgressDialog->setValue(1);
        mProgressDialog->setFinished(true);


            mProgressDialog->setStatusText(tr("Process finished."));
            int millis, hours, min, seg;
            millis = mTime->elapsed();
            hours = millis/3600000;
            min = (millis-(3600000*hours))/60000;
            seg = (millis-(3600000*hours)-(60000*min))/1000;

            lbTotalTime->setText(QString::number(hours)+" h "+QString::number(min)+" min "+QString::number(seg)+" seg");

            QMessageBox msgBox(this);
            msgBox.setWindowTitle("Results location");
            msgBox.setText("Currently it is not posible to write gDel3D resuls");
            msgBox.exec();


    }
    disconnect(mProgressDialog,SIGNAL(cancel()),mGDel3DProcess,SLOT(stop()));
}

void MainWindow::on_PCL3DProcess_finished(){
    if (!mPCL3DProcess->isStopped()) {
        QFileInfo inputFileInfo(mSourceFile);
        QFileInfo outputFileInfo(inputFileInfo.absolutePath()+"/"+inputFileInfo.baseName()+"_PCL3DResult.obj");

        mProgressDialog->setRange(0,1);
        mProgressDialog->setValue(1);
        mProgressDialog->setFinished(true);

        if (outputFileInfo.exists() || !mChSaveResults->isChecked()) {
            mProgressDialog->setStatusText(tr("Process finished."));
            int millis, hours, min, seg;
            millis = mTime->elapsed();
            hours = millis/3600000;
            min = (millis-(3600000*hours))/60000;
            seg = (millis-(3600000*hours)-(60000*min))/1000;
            lbTotalTime->setText(QString::number(hours)+" h "+QString::number(min)+" min "+QString::number(seg)+" seg");
            if (mChSaveResults->isChecked()) {
                if (mChShowResults->isChecked()) {
                    mCCVWResults->addToDB(QStringList() << outputFileInfo.absoluteFilePath());
                    mainTabWidget->setTabEnabled(1, true);
                    mainTabWidget->setCurrentIndex(1);
                }else {
                    QMessageBox msgBox(this);
                    msgBox.setWindowTitle("Results location");
                    msgBox.setText("Current results are located on the path:");
                    msgBox.setInformativeText(outputFileInfo.absoluteFilePath());
                    msgBox.exec();
                }
            }
        }else {
            mProgressDialog->setStatusText(tr("Process failed."));
        }

    }
    disconnect(mProgressDialog,SIGNAL(cancel()),mPCL3DProcess,SLOT(stop()));
}

void MainWindow::on_PCLPoissonProcess_finished(){
    if (!mPCLPoissonProcess->isStopped()) {
        QFileInfo inputFileInfo(mSourceFile);
        QFileInfo outputFileInfo(inputFileInfo.absolutePath()+"/"+inputFileInfo.baseName()+"_PCLPoissonResult.obj");

        mProgressDialog->setRange(0,1);
        mProgressDialog->setValue(1);
        mProgressDialog->setFinished(true);

        if (outputFileInfo.exists() || !mChSaveResults->isChecked()) {
            mProgressDialog->setStatusText(tr("Process finished."));
            int millis, hours, min, seg;
            millis = mTime->elapsed();
            hours = millis/3600000;
            min = (millis-(3600000*hours))/60000;
            seg = (millis-(3600000*hours)-(60000*min))/1000;
            lbTotalTime->setText(QString::number(hours)+" h "+QString::number(min)+" min "+QString::number(seg)+" seg");
            if (mChSaveResults->isChecked()) {
                if (mChShowResults->isChecked()) {
                    mCCVWResults->addToDB(QStringList() << outputFileInfo.absoluteFilePath());
                    mainTabWidget->setTabEnabled(1, true);
                    mainTabWidget->setCurrentIndex(1);
                }else {
                    QMessageBox msgBox(this);
                    msgBox.setWindowTitle("Results location");
                    msgBox.setText("Current results are located on the path:");
                    msgBox.setInformativeText(outputFileInfo.absoluteFilePath());
                    msgBox.exec();
                }
            }
        }else {
            mProgressDialog->setStatusText(tr("Process failed."));
        }

    }
    disconnect(mProgressDialog,SIGNAL(cancel()),mPCLPoissonProcess,SLOT(stop()));
}
