#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "ccviewerwrapper.h"
#include "ccCommon.h"
#include "ccGLWindow.h"
#include <ccPointCloud.h>
#include <cc2DLabel.h>
#include <cc2DViewportLabel.h>
#include <QDockWidget>
#include "ProgressDialog.h"
#include <QComboBox>
#include <QPushButton>
#include "TriangulationProcess/Las2tinProcess.h"
#include "TriangulationProcess/Fade25DProcess.h"
#include "TriangulationProcess/CGALTriang.h"
#include "TriangulationProcess/TrianglePlusPlusProcess.h"
#include "TriangulationProcess/TriangleCCProcess.h"
#include "TriangulationProcess/PCLTriangl3DProcess.h"
#include "TriangulationProcess/PCLPoissonProcess.h"

//#include <QTime>
#include <QLabel>
#include <QGroupBox>
#include <QRadioButton>
#include <QCheckBox>
#include <ClusteringProcess.h>
#include <QSpinBox>
#include "ProcessManager/MultiProcess.h"
#define MAINWINDOWDELAUNAYEVALUATOR_PROGRAM_NAME         "DelaunayEvaluator"
#define MAINWINDOWDELAUNAYEVALUATOR_PROGRAM_TAG          "DelaunayEvaluator"
#define MAINWINDOWDELAUNAYEVALUATOR_VERSION              "0.3"
#define MAINWINDOWDELAUNAYEVALUATOR_BUILD_NUMBER         "3"
#define MAINWINDOWDELAUNAYEVALUATOR_RELEASE_TYPE         "(beta)"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    int *mReadMillis, *mWriteMillis, *mTriangMillis,*mPointsCount;

private slots:
    void on_actionExit_triggered();

    void on_actionLoad_data_triggered();

private:

    Ui::MainWindow *ui;
    ccViewerWrapper *mCCVWDataSet, *mCCVWResults;
    QTabWidget *mainTabWidget;
    QDockWidget *mDockResults, *mDockSettings;
    ProgressDialog *mProgressDialog;
    QTextEdit *mConsole;
    QString mSourceFile;
    QComboBox *mCbTool;
    QPushButton *mPbCompute, *mPbAddConstraintLines, *mPbAddExclusionArea;
    Las2tinProcess *mLas2tinProcess;
    Fade25DProcess *mFade25DProcess;
    MultiProcess *mGDel3DProcess;
    CGALTriang *mCGALProcess;
    TrianglePlusPlusProcess *mTriangle_PP_Process;
    TriangleCCProcess *mTriangleCCProcess;
    PCLTriangl3DProcess *mPCL3DProcess;
    PCLPoissonProcess *mPCLPoissonProcess;
    QTime *mTime;
    QLabel *lbReadTime,*lbTrianglTime,*lbWriteTime, *lbClusteringTime,*lbNumPoints,*lbNumTriangles,*lbNumEdges,*lbTotalTime;
    QGroupBox *mGbFade25DSettings, *mGbCGALSettings,*mTimeResults, *mInputData,*mResultData;
    QRadioButton *mRbKeepDelaunay, *mRbIgnoreDelaunay;
    QCheckBox *mChRefineDelaunay,*mChClusterize, *mChShowResults, *mChPreviewData, *mChSaveResults;
    QString mConstraintFile,mExclusionAreaFile;
    ClusteringProcess *mclusteringProcess;
    QSpinBox *mClusterEdge;

    bool removeDir(QString dirName);
    MultiProcess *mParallelTriangulationProcess;
    QStringList mMultiprocessSourceList;
    QFrame *mClusterizeFrame;
//    void writeResults(QString filename, QString triangMethod,int *points, int *readMillis,int *writeMillis, int *triangMillis);
private slots:
    void  on_pbCompute_triggered();
    void manageProccesStdOutput(QString data);
    void manageProccesErrorOutput(QString data);
    void onError(int code, QString cause);
    void on_las2tinProcess_finished();
    void on_Fade25DProcess_finished();
    void on_PbAddConstraintLines_triggered();
    void on_PbAddExclusionArea_triggered();
    void on_CbTool_selectionChanged(int currentIndex);
    void on_CGALProcess_finished();
    void on_actionAbout_triggered();
    void on_Triangle_PP_Process_finished();
    void on_Triangle_CC_Process_finished();
    void on_ClusteringProcess_finished();
    void on_parallelTriangulationProcess_finished();
    void on_gDel3D_Process_finished();
    void on_PCL3DProcess_finished();
    void on_PCLPoissonProcess_finished();

};

#endif // MAINWINDOW_H
