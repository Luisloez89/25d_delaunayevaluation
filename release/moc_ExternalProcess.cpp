/****************************************************************************
** Meta object code from reading C++ file 'ExternalProcess.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../ProcessManager/ExternalProcess.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ExternalProcess.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ExternalProcess[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      17,   16,   16,   16, 0x0a,
      37,   24,   16,   16, 0x09,
      74,   69,   16,   16, 0x09,
      91,   16,   16,   16, 0x09,
     103,   16,   16,   16, 0x08,
     119,   16,   16,   16, 0x08,
     142,  137,   16,   16, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ExternalProcess[] = {
    "ExternalProcess\0\0stop()\0commandError\0"
    "onError(QProcess::ProcessError)\0path\0"
    "onError(QString)\0onTimeout()\0"
    "on_newStdData()\0on_newErrorData()\0"
    "code\0on_mProcessFinished(int)\0"
};

void ExternalProcess::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ExternalProcess *_t = static_cast<ExternalProcess *>(_o);
        switch (_id) {
        case 0: _t->stop(); break;
        case 1: _t->onError((*reinterpret_cast< QProcess::ProcessError(*)>(_a[1]))); break;
        case 2: _t->onError((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->onTimeout(); break;
        case 4: _t->on_newStdData(); break;
        case 5: _t->on_newErrorData(); break;
        case 6: _t->on_mProcessFinished((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ExternalProcess::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ExternalProcess::staticMetaObject = {
    { &Process::staticMetaObject, qt_meta_stringdata_ExternalProcess,
      qt_meta_data_ExternalProcess, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ExternalProcess::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ExternalProcess::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ExternalProcess::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ExternalProcess))
        return static_cast<void*>(const_cast< ExternalProcess*>(this));
    return Process::qt_metacast(_clname);
}

int ExternalProcess::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Process::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
