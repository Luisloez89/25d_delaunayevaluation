/****************************************************************************
** Meta object code from reading C++ file 'MultiProcess.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../ProcessManager/MultiProcess.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MultiProcess.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MultiProcess[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x0a,
      32,   21,   13,   13, 0x08,
      53,   13,   13,   13, 0x08,
      92,   74,   13,   13, 0x08,
     126,   13,   13,   13, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MultiProcess[] = {
    "MultiProcess\0\0stop()\0code,cause\0"
    "onError(int,QString)\0onAProcessFinished()\0"
    "step,childMessage\0OnChildStatusChanged(int,QString)\0"
    "OnChildStatusChangedNext()\0"
};

void MultiProcess::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MultiProcess *_t = static_cast<MultiProcess *>(_o);
        switch (_id) {
        case 0: _t->stop(); break;
        case 1: _t->onError((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 2: _t->onAProcessFinished(); break;
        case 3: _t->OnChildStatusChanged((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 4: _t->OnChildStatusChangedNext(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MultiProcess::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MultiProcess::staticMetaObject = {
    { &Process::staticMetaObject, qt_meta_stringdata_MultiProcess,
      qt_meta_data_MultiProcess, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MultiProcess::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MultiProcess::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MultiProcess::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MultiProcess))
        return static_cast<void*>(const_cast< MultiProcess*>(this));
    return Process::qt_metacast(_clname);
}

int MultiProcess::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Process::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
