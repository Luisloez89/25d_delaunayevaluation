/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../mainwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      20,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x08,
      38,   11,   11,   11, 0x08,
      69,   11,   11,   11, 0x08,
      99,   94,   11,   11, 0x08,
     131,   94,   11,   11, 0x08,
     176,  165,   11,   11, 0x08,
     197,   11,   11,   11, 0x08,
     226,   11,   11,   11, 0x08,
     255,   11,   11,   11, 0x08,
     291,   11,   11,   11, 0x08,
     338,  325,   11,   11, 0x08,
     370,   11,   11,   11, 0x08,
     396,   11,   11,   11, 0x08,
     423,   11,   11,   11, 0x08,
     457,   11,   11,   11, 0x08,
     491,   11,   11,   11, 0x08,
     523,   11,   11,   11, 0x08,
     566,   11,   11,   11, 0x08,
     595,   11,   11,   11, 0x08,
     622,   11,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0\0on_actionExit_triggered()\0"
    "on_actionLoad_data_triggered()\0"
    "on_pbCompute_triggered()\0data\0"
    "manageProccesStdOutput(QString)\0"
    "manageProccesErrorOutput(QString)\0"
    "code,cause\0onError(int,QString)\0"
    "on_las2tinProcess_finished()\0"
    "on_Fade25DProcess_finished()\0"
    "on_PbAddConstraintLines_triggered()\0"
    "on_PbAddExclusionArea_triggered()\0"
    "currentIndex\0on_CbTool_selectionChanged(int)\0"
    "on_CGALProcess_finished()\0"
    "on_actionAbout_triggered()\0"
    "on_Triangle_PP_Process_finished()\0"
    "on_Triangle_CC_Process_finished()\0"
    "on_ClusteringProcess_finished()\0"
    "on_parallelTriangulationProcess_finished()\0"
    "on_gDel3D_Process_finished()\0"
    "on_PCL3DProcess_finished()\0"
    "on_PCLPoissonProcess_finished()\0"
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->on_actionExit_triggered(); break;
        case 1: _t->on_actionLoad_data_triggered(); break;
        case 2: _t->on_pbCompute_triggered(); break;
        case 3: _t->manageProccesStdOutput((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: _t->manageProccesErrorOutput((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 5: _t->onError((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 6: _t->on_las2tinProcess_finished(); break;
        case 7: _t->on_Fade25DProcess_finished(); break;
        case 8: _t->on_PbAddConstraintLines_triggered(); break;
        case 9: _t->on_PbAddExclusionArea_triggered(); break;
        case 10: _t->on_CbTool_selectionChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->on_CGALProcess_finished(); break;
        case 12: _t->on_actionAbout_triggered(); break;
        case 13: _t->on_Triangle_PP_Process_finished(); break;
        case 14: _t->on_Triangle_CC_Process_finished(); break;
        case 15: _t->on_ClusteringProcess_finished(); break;
        case 16: _t->on_parallelTriangulationProcess_finished(); break;
        case 17: _t->on_gDel3D_Process_finished(); break;
        case 18: _t->on_PCL3DProcess_finished(); break;
        case 19: _t->on_PCLPoissonProcess_finished(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MainWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 20)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 20;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
